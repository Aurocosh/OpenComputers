-----------------------------------------------------------------------------
-- Imports and dependencies
-----------------------------------------------------------------------------
local term = require("term")
local filesystem = require("filesystem")
local serialization = require("serialization")

-----------------------------------------------------------------------------
-- Module declaration
-----------------------------------------------------------------------------
local logf = {}

-----------------------------------------------------------------------------
-- Constants
-----------------------------------------------------------------------------

logf.logfLevelName = {
    ERROR = 1,
    WARNING = 2,
    INFO = 3,
	DEBUG = 4,
	VERBOSE = 5,}
    
-----------------------------------------------------------------------------
-- Persist data
-----------------------------------------------------------------------------

local logfLevel = 5
local logfLogToFile = true
local logfLogToConsole = true

-----------------------------------------------------------------------------
-- Private functions
-----------------------------------------------------------------------------

local function writeLog(logString)
    if logfLogToConsole then
        term.write(logString)
    end

    if logfLogToFile then
        if not filesystem.exists('/data') then
            filesystem.makeDirectory('/data')
        end
        
        local f = io.open('/data/log', 'a')
        f:write(logString)
        f:close()
    end
end

local function writeFormatted(level, module, fmt, ...)
    local logMsg = string.format(fmt, ...)
    local currentTime = os.date("%X%p")
	logString = string.format('%s %s [%s]: %s', currentTime, level, module, logMsg)
	writeLog(logString.."\n")
end

-----------------------------------------------------------------------------
-- Public functions
-----------------------------------------------------------------------------

function logf.line(level, module, fmt, ...)
	if logf.logfLevelName[level] == nil or logfLevel < logf.logfLevelName[level] then
		return
	end
	writeFormatted(level, module, fmt, ...)
end

function logf.table(level, module, fmt, value, ...)
	if logf.logfLevelName[level] == nil or logfLevel < logf.logfLevelName[level] then
		return
	end
    local valueString = serialization.serialize(value,true)
	writeFormatted(level, module, fmt, valueString, ...)
end

return logf