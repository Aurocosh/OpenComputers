-----------------------------------------------------------------------------
-- Imports and dependencies
-----------------------------------------------------------------------------
local gis = require("gis")
local logf = require("logf")
local util = require("util")
local robot = require("robot")
local sides = require("sides")
local wsides = require("wsides")
local vector = require("vector")
local component = require("component")
local pathConstruct = require("pathConstruct")

-----------------------------------------------------------------------------
-- Module declaration
-----------------------------------------------------------------------------
local nav = {}

-----------------------------------------------------------------------------
-- Constants
-----------------------------------------------------------------------------

--- Lookup table for calculating turns. Also, I couldn't resist the name

nav.sideLookup = {
    -- Negative values are left turns, positive values are right turns
    turn = {
        [sides.north] = {
            [sides.south] = 2,
            [sides.east] = 1,
            [sides.west] = -1
        },
        [sides.south] = {
            [sides.north] = 2,
            [sides.east] = -1,
            [sides.west] = 1
        },
        [sides.east] = {
            [sides.north] = -1,
            [sides.south] = 1,
            [sides.west] = 2
        },
        [sides.west] = {
            [sides.north] = 1,
            [sides.south] = -1,
            [sides.east] = 2
        }
    },

    -- Translation table for turning a local direction into a cardinal/global direction
    translation = {
        [sides.north] = {
            [sides.front] = sides.north,
            [sides.back] = sides.south,
            [sides.left] = sides.west,
            [sides.right] = sides.east
        },
        [sides.south] = {
            [sides.front] = sides.south,
            [sides.back] = sides.north,
            [sides.left] = sides.east,
            [sides.right] = sides.west
        },
        [sides.east] = {
            [sides.front] = sides.east,
            [sides.back] = sides.west,
            [sides.left] = sides.north,
            [sides.right] = sides.south
        },
        [sides.west] = {
            [sides.front] = sides.west,
            [sides.back] = sides.east,
            [sides.left] = sides.south,
            [sides.right] = sides.north
        }
    },

    offsets = {
        [sides.down] = { 0, -1, 0 },
        [sides.up] = { 0, 1, 0 },
        [sides.north] = { 0, 0, -1 },
        [sides.south] = { 0, 0, 1 },
        [sides.west] = { -1, 0, 0 },
        [sides.east] = { 1, 0, 0 }
    },

    valid = { sides.down, sides.up, sides.north, sides.south, sides.west, sides.east }
}

-----------------------------------------------------------------------------
-- Persist data
-----------------------------------------------------------------------------
local _position = { x = 0, y = 0, z = 0, facing = sides.north }
local _diggingAllowed = false
local _throwErrorIfCannotMove = false
local _suckItemsWhileMoving = false
local _robot = component.robot

-----------------------------------------------------------------------------
-- Private functions
-----------------------------------------------------------------------------

local function repeatAction(times, action, ...)
    logf.line("VERBOSE","Nav","Repeating action "..times.." times")
    for i = 1, times do
        local ok, err = action(...)

        if not ok then
            return ok, err
        end
    end
    return true
end

local function tryAction(action, ...)
    logf.line("VERBOSE","Nav","Trying action")
    local ok, err = action(...)
    if not ok then
        logf.line("VERBOSE","Nav","Trying error "..err)
        error(err, 0)
    end
end

local function protected(body, ...)
    local ok, err = pcall(body, ...)
    return ok or nil, err
end

--- Attempts to move robot 50 times while digging or attacking whatewer is in the way
local function movePersistently(movementFunction, side)
    local sideToSwing = side == sides.back and sides.front or side
    for i = 1, 50 do
        if movementFunction() then
            return true
        elseif not _diggingAllowed then
            logf.line("WARNING","Nav","Movement failed")
            return false, "Unable to move"
        elseif _diggingAllowed then
            logf.line("VERBOSE","nav","Movement failed. Swinging "..wsides[sideToSwing])
            if side == sides.back then
                nav.turnAround()
            end        
            local result, reason = _robot.swing(sideToSwing)
            logf.line("VERBOSE","nav","Swing result: "..tostring(result))
            logf.line("VERBOSE","nav","Swing data: "..reason)
            os.sleep(0.25)
        else
            local _, what = _robot.detect(sideToSwing)
            logf.line("VERBOSE","nav","Detecting "..wsides[sideToSwing])
            if(what == "entity") then
                _robot.swing(sideToSwing)
                os.sleep(0.25)
            end
        end
        if side == sides.back then
            nav.turnAround()
        end  
        logf.line("VERBOSE","Nav","Movement attempt "..i.." failed")
    end
    if(_throwErrorIfCannotMove) then
        error("Cannot move")    
    end
    if(_suckItemsWhileMoving) then
        _robot.suck(sides.front)
        _robot.suckDown()
    end    
    return false
end

-----------------------------------------------------------------------------
-- Public functions
-----------------------------------------------------------------------------

function nav.setDiggingAllowed(diggingAllowed)
    _diggingAllowed = util.toBool(diggingAllowed)
end

function nav.setThrowErrorIfCannotMove(throwError)
    _throwErrorIfCannotMove = util.toBool(throwError)
end

function nav.setSuckItemsWhileMoving(suckItems)
    _suckItemsWhileMoving = util.toBool(suckItems)
end

function nav.turnLeft()
    robot.turnLeft()
    _position.facing = nav.sideLookup.translation[nav.getFacing()][sides.left]
    return true
end

function nav.turnRight()
    robot.turnRight()
    _position.facing = nav.sideLookup.translation[nav.getFacing()][sides.right]
    return true
end

function nav.turnAround()
    local turn = math.random() < 0.5 and nav.turnLeft or nav.turnRight

    return protected(function()
        tryAction(turn)
        tryAction(turn)
    end)
end

function nav.getFacing()
    return _position.facing
end

function nav.getPosition()
    return _position.x, _position.y, _position.z
end

--- Returns which way a particular side of the robot is facing
-- For example, if the robot is facing north, passing sides.left would return sides.west
-- @param side The side of the robot
-- @returns The facing of the robot's side
function nav.getFacingFromSide(side)
    -- Don't translate up/down
    if side == sides.up or side == sides.down then
        return side
    end

    assert(side, "Invalid side")
    local facing = nav.getFacing()
    local lookup =  nav.sideLookup.translation[facing][side]
    assert(lookup, "Invalid side")

    return lookup
end

function nav.forward()
    logf.line("VERBOSE","Nav","Moving forward")
    local moved = movePersistently(robot.forward, sides.front)
    if not moved then
        return false
    end
    
    local facing = nav.getFacing()
    if facing == sides.posx then _position.x = _position.x + 1
    elseif facing == sides.negx then _position.x = _position.x - 1
    elseif facing == sides.posz then _position.z = _position.z + 1
    elseif facing == sides.negz then _position.z = _position.z - 1
    end
    return true
end

--- Moves the robot back one space
-- Use this and avoid robot.back() to keep track the robot's position
-- @return true on success, or nil, and an optional error message
function nav.back()
    logf.line("VERBOSE","Nav","Moving back")
    local moved = movePersistently(robot.back, sides.back)
    if not moved then
        return false
    end
    
    local facing = nav.getFacing()
    if facing == sides.posx then _position.x = _position.x - 1
    elseif facing == sides.negx then _position.x = _position.x + 1
    elseif facing == sides.posz then _position.z = _position.z - 1
    elseif facing == sides.negz then _position.z = _position.z + 1
    end
    return true
end

--- Moves the robot up one space
-- Use this and avoid robot.up() to keep track the robot's position
-- @return true on success, or nil, and an optional error message
function nav.up()
    logf.line("VERBOSE","Nav","Moving up")
    local moved = movePersistently(robot.up, sides.up)
    if not moved then
        return false
    end

    _position.y = _position.y + 1
    return true
end

--- Moves the robot down one space
-- Use this and avoid robot.down() to keep track the robot's position
-- @return true on success, or nil, and an optional error message
function nav.down()
    logf.line("VERBOSE","Nav","Moving down")
    local moved = movePersistently(robot.down, sides.down)
    if not moved then
        return false
    end
    
    _position.y = _position.y - 1
    return true
end

--- Turns the robot to face the side constant. front/back/left/right are interpreted as south/north/west/east as per
-- the definition in the sides constant. The facing is relative to the robot's tracking data.
-- @param side the side the robot will turn to face
-- @return true on success, or nil, and an optional error message
function nav.faceSide(side)
    logf.line("VERBOSE",'Nav','Preparing to turn towards side '..sides[side])
    local oldSide = nav.getFacing()
    logf.line("VERBOSE",'Nav','Current side '..sides[oldSide])
    logf.line("VERBOSE",'Nav','Sides are equal '..tostring(oldSide == side).." values "..side.." "..oldSide)
    if (oldSide == side) then
        logf.line("VERBOSE",'Nav','Alredy facing side '..sides[oldSide])
        return true
    end
    
    local turn = nav.sideLookup.turn[oldSide] and nav.sideLookup.turn[oldSide][side]
    assert(turn, "Unable to face side: " .. side or "nil")
    
    logf.line("VERBOSE",'Nav','Turning towards side '..sides[side].." turn value "..turn)
    return turn == 2 and nav.turnAround() or turn == -1 and nav.turnLeft() or turn == 1 and nav.turnRight()
end

--- Moves the robot in a direction a certain distance relative to his tracking data
-- @param direction The direction the robot will travel
-- @param distance The distance in blocks the robot will travel
-- @return true on success, or nil, and an optional error message
function nav.move(direction, distance)
    logf.line("VERBOSE",'Nav','Moving in direction '..sides[direction].." distance "..distance)

    direction, distance = tonumber(direction), tonumber(distance)
    assert(direction and distance, "direction and distance must be a number")

    if distance <= 0 then
        return true
    end

    return protected(function()
        local moveFunc

        if direction == sides.up then
            moveFunc = nav.up
        elseif direction == sides.down then
            moveFunc = nav.down
        else
            assert(nav.sideLookup.turn[direction], "invalid direction")
            tryAction(nav.faceSide, direction)
            moveFunc = nav.forward
        end

        tryAction(repeatAction, distance, moveFunc)
    end)
end

function nav.step(direction)
    logf.line("VERBOSE",'Nav','Stepping in direction '..sides[direction])

    direction = tonumber(direction)
    assert(direction and distance, "direction must be a number")

    return protected(function()
        local moveFunc

        if direction == sides.up then
            moveFunc = nav.up
        elseif direction == sides.down then
            moveFunc = nav.down
        else
            assert(nav.sideLookup.turn[direction], "invalid direction")
            tryAction(nav.faceSide, direction)
            moveFunc = nav.forward
        end

        tryAction(moveFunc)
    end)
end

--- Makes the robot move along the X axis until he reaches a given X
-- @param x The X the robot will travel to
-- @return true on success, or nil, and an optional error message
function nav.moveX(x)
    x = tonumber(x)
    assert(x, "x must be a number")

    local ourX = nav.getPosition()
    return nav.move(ourX < x and sides.posx or sides.negx, math.abs(ourX - x))
end

--- Makes the robot move along the Y axis until he reaches a given Y
-- @param y The Y the robot will travel to
-- @return true on success, or nil, and an optional error message
function nav.moveY(y)
    logf.line("VERBOSE",'Nav','Moving along y')
    y = tonumber(y)
    assert(y, "y must be a number")

    local _, ourY = nav.getPosition()
    logf.line("VERBOSE",'Nav','Current position '..ourY)
    

    return nav.move(ourY < y and sides.up or sides.down, math.abs(ourY - y))
end

--- Makes the robot move along the Z axis until he reaches a given Z
-- @param z The Z the robot will travel to
-- @return true on success, or nil, and an optional error message
function nav.moveZ(z)
    z = tonumber(z)
    assert(z, "z must be a number")

    local _, _, ourZ = nav.getPosition()
    return nav.move(ourZ < z and sides.posz or sides.negz, math.abs(ourZ - z))
end

--- Makes the robot move along the Z and X axes until he reaches the given destination. The robot will attempt to move
-- on whichever axes he is already facing.
-- @param x The X the robot will travel to
-- @param z The Z the robot will travel to
-- @return true on success, or nil, and an optional error message
function nav.moveXZ(x, z)
    x, z = tonumber(x), tonumber(z)
    assert(x and z, "x and z must be a number")

    return protected(function()
        local _, _, ourZ = nav.getPosition()
        local zFacing = ourZ < z and sides.posz or sides.negz
        if zFacing == nav.getFacing() then
            tryAction(nav.moveZ, z)
            tryAction(nav.moveX, x)
        else
            tryAction(nav.moveX, x)
            tryAction(nav.moveZ, z)
        end
    end)
end

function nav.gisNavigate(targetName)
    local target = gis.getCoordinates(targetName)
    if target == nil then
        logf.line("WARNING","nav","Unknown coordinates: "..targetName)
        return false, "Unknown coordinates: "..targetName
    end
    return nav.navigate(target)
end

function nav.navigate(target)
    local myX, myY, myZ = nav.getPosition()
    local current = vector.new(myX, myY, myZ)

    local path = pathConstruct.constructDirectPath(current, target)
    for _,v in ipairs(path) do 
        logf.line("DEBUG","nav","Moving. Direction: "..wsides[v.direction].." distance: "..v.distance)
        result, reason = nav.move(v.direction, v.distance)
        if not result then
            return false, reason
        end
    end
    return true
end

function nav.gisNavigateCoroutine(targetName)
    local target = gis.getCoordinates(targetName)
    if target == nil then
        logf.line("WARNING","nav","Unknown coordinates: "..targetName)
        return false, "Unknown coordinates: "..targetName
    end
    return nav.navigateCoroutine(target)
end

function nav.navigateCoroutine(target)
    local myX, myY, myZ = nav.getPosition()
    local current = vector.new(myX, myY, myZ)

    local path = pathConstruct.constructDirectPath(current, target)
    for _,v in ipairs(path) do 
        logf.line("DEBUG","nav","Moving. Direction: "..wsides[v.direction].." distance: "..v.distance)
        result, reason = nav.move(v.direction, v.distance)
        if not result then
            return false, reason
        end
        coroutine.yield()
    end
    return true
end

--- Setings the robots position and facing in the tracking data. Note that this does not physically move the robot.
-- This is useful for loading position and facing from the navigation component, resetting the robot's origin, etc.
-- @param x The X position of the robot
-- @param y The Y position of the robot
-- @param z The Z position of the robot
-- @param facing a sides constant representing the direction the robot is facing
function nav.setPosition(x, y, z, facing)
    x, y, z, facing = tonumber(x), tonumber(y), tonumber(z), tonumber(facing)
    assert(x and y and z, "Invalid x,y,z")
    assert(nav.sideLookup.turn[facing], "Invalid facing")
    _position.x, _position.y, _position.z = x, y, z
    _position.facing = facing
end

--- Gets the euclidean distance between two 3 dimensional points
-- @param x1 The X of the first point
-- @param y1 The Y of the first point
-- @param z1 The Z of the first point
-- @param x2 The X of the second point
-- @param y2 The Y of the second point
-- @param z2 The Z of the second point
-- @return The distance between the two points
function nav.distance(x1, y1, z1, x2, y2, z2)
    return math.sqrt(nav.distancesq(x1, y1, z1, x2, y2, z2))
end

--- Gets the squared (or manhattan) distance between two 3 dimensional points
-- @param x1 The X of the first point
-- @param y1 The Y of the first point
-- @param z1 The Z of the first point
-- @param x2 The X of the second point
-- @param y2 The Y of the second point
-- @param z2 The Z of the second point
-- @return The squared distance between the two points
function nav.distancesq(x1, y1, z1, x2, y2, z2)
    return math.abs(x1 - x2) + math.abs(y1 - y2) + math.abs(z1 - z2)
end

--- Gets the position of the six neighboing blocks of a point
-- @param x The origin X to get the neighbors of
-- @param y The origin Y to get the neighbors of
-- @param z The origin Z to get the neighbors of
-- @return A list of x, y, z, and sides constant for each neighbor describing its direction in relation to the origin
function nav.getBlockNeighbors(x, y, z)
    return {
        { x - 1, y, z, sides.negx },
        { x + 1, y, z, sides.posx },
        { x, y, z - 1, sides.negz },
        { x, y, z + 1, sides.posz },
        { x, y - 1, z, sides.negy },
        { x, y + 1, z, sides.posy }
    }
end

return nav