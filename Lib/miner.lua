-----------------------------------------------------------------------------
-- Imports and dependencies
-----------------------------------------------------------------------------
local nav = require("nav")
local logf = require("logf")
local robot = require("robot")
local sides = require("sides")
local component = require("component")

-----------------------------------------------------------------------------
-- Module declaration
-----------------------------------------------------------------------------
local miner = {}

-----------------------------------------------------------------------------
-- Constants
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Persist data
-----------------------------------------------------------------------------
local robotComponent = component.robot

-----------------------------------------------------------------------------
-- Private functions
-----------------------------------------------------------------------------


-----------------------------------------------------------------------------
-- Public functions
-----------------------------------------------------------------------------
function miner.step()
    nav.forward()
    miner.clearBlock(sides.down)
    miner.clearBlock(sides.up)
end

function miner.suck(side)
    repeat until not robotComponent.suck(side)
end

function miner.breakBlock(side, cannotRetry)
    local result, reason = robotComponent.swing(side)
        if not result then
        local _, what = robotComponent.detect(side)
        if cannotRetry and what ~= "air" and what ~= "entity" then
            return false
        end
    end
    return true
end

function miner.clearBlock(side, cannotRetry)
    miner.suck(side)
    local result, reason = robotComponent.swing(side)
        if not result then
        local _, what = robotComponent.detect(side)
        if cannotRetry and what ~= "air" and what ~= "entity" then
            return false
        end
    end
    return true
end

function miner.dropItems()
    logf.logLine("INFO",'Miner','Dropping off items')
    for slot = 1, 15 do
        if robot.count(slot) > 0 then
            robot.select(slot)
            local wait = 1
            repeat
                if not robot.drop() and not robot.dropDown() then
                    os.sleep(wait)
                    wait = math.min(10, wait + 1)
                end
            until robot.count(slot) == 0
        end
    end
    robot.select(1)
    return true
end

function miner.deliverItems(side)
    local allEmpty = true;
    for i = 14, 15 do
        allEmpty = allEmpty and robotComponent.count(i) == 0
    end
    if allEmpty then
        return
    end
    
    logf.logLine("INFO",'Miner','Delivering items')
    local x,y,z = nav.getPosition()
    local facing = nav.getFacing()
    side = side or sides.south
    
    nav.moveXZ(0,0)
    nav.moveY(0)    
    nav.faceSide(side)
    miner.dropItems()
    nav.moveY(y)
    nav.moveXZ(x,z)
    nav.faceSide(facing)
    return
end

function miner.descend()
    while not robot.detectDown() do
        if not nav.down() then
            return false,"Unable to move"
        end
    end
    return true
end

return miner