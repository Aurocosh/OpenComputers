-----------------------------------------------------------------------------
-- Imports and dependencies
-----------------------------------------------------------------------------

local nav = require("nav")
local logf = require("logf")
local wshell = require("wshell")
local vector = require("vector")
local persist = require("persist")
local excavate = require("excavate")
local serialization = require("serialization")

-----------------------------------------------------------------------------
-- Module declaration
-----------------------------------------------------------------------------
local tasks = {}

-----------------------------------------------------------------------------
-- Constants
-----------------------------------------------------------------------------
local _taskHandlers = {}
_taskHandlers.navigate = nav.navigateCoroutine
_taskHandlers.gisNavigate = nav.gisNavigateCoroutine
_taskHandlers.rgisDig = excavate.rgisDig

-----------------------------------------------------------------------------
-- Persist data
-----------------------------------------------------------------------------
local _tasks = {}
local _newTasks = {}
local _delayedTasks = {}

-----------------------------------------------------------------------------
-- Private functions
-----------------------------------------------------------------------------

local function processNewTasks()
    for _,v in pairs(_newTasks) do
        table.insert(_tasks,v)
    end
    _newTasks = {}
end

local function processDelayedTasks()
    for i = #_delayedTasks, 1, -1 do
        table.insert(_tasks,1,_delayedTasks[i])        
    end
    _delayedTasks = {}
end
-----------------------------------------------------------------------------
-- Public functions
-----------------------------------------------------------------------------
function tasks.addTask(command, parameters, delayed)
    if delayed == nil then delayed = false end
    
    local task = {
        command = command,
        parameters = parameters
    }
    
    if delayed then
        table.insert(_delayedTasks, task);
    else
        table.insert(_newTasks, task);
    end
end

function tasks.getTaskList()
    return _tasks, _newTasks, _delayedTasks
end

function tasks.getNextTask()
    if #_tasks > 0 or #_newTasks > 0 or #_delayedTasks > 0 then 
        processNewTasks()
        processDelayedTasks()
    
        local taskIndex = #_tasks
        local task = _tasks[taskIndex]
        table.remove(_tasks, taskIndex);
        
        local taskHandler = _taskHandlers[task.command]
        
        logf.line("DEBUG","tasks","Task "..task.command.." prepared.")
        return taskHandler, task.parameters
    end
    return nil
end

function tasks.taskCount()
    return #_tasks + #_newTasks + #_delayedTasks
end

function tasks.newTaskCount()
    return #_newTasks
end

function tasks.queuedTaskCount()
    return #_tasks
end

function tasks.delayedTaskCount()
    return #_delayedTasks
end

function tasks.clearTasks()
    _tasks = {}
    _newTasks = {}
    _delayedTasks = {}
    return true
end

function tasks.saveTasks()
    local planned = {
        tasks = _tasks,
        new = _newTasks,
        delayed = _delayedTasks
    }
    return persist.saveSharedData("plannedTasks", planned)
end

function tasks.loadTasks()
    planned = persist.loadSharedData("plannedTasks")
    _tasks = planned.tasks or {}
    _newTasks = planned.new or {}
    _delayedTasks = planned.delayed or {}
    return true
end

return tasks