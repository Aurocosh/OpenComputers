-----------------------------------------------------------------------------
-- Imports and dependencies
-----------------------------------------------------------------------------
local term = require("term")
local serialization = require("serialization")

-----------------------------------------------------------------------------
-- Module declaration
-----------------------------------------------------------------------------
local wterm = {}

-----------------------------------------------------------------------------
-- Constants
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Persist data
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Private functions
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Public functions
-----------------------------------------------------------------------------

function wterm.write(text, newLine, wrap)
    if newLine == nil then newLine = true end
    if wrap == nil then wrap = true end

    local suffix = newLine and "\n" or ""
    return term.write(text..suffix, wrap)
end

function wterm.writeError(errorText)
    local prefix = "Error occured: "
    local suffix = "\n"
    io.stderr:write(prefix..errorText..suffix)
    return true
end

function wterm.writeTable(value, pretty, newLine, wrap)
    if pretty == nil then pretty = true end
    if newLine == nil then newLine = true end
    if wrap == nil then wrap = true end
    
    local valueString = serialization.serialize(value, pretty)
    return wterm.write(valueString,newLine,wrap)
end

function wterm.newLine()
    return term.write("\n")
end

return wterm