-----------------------------------------------------------------------------
-- Imports and dependencies
-----------------------------------------------------------------------------
local logf = require("logf")
local filesystem = require("filesystem")
local serialization = require("serialization")

-----------------------------------------------------------------------------
-- Module declaration
-----------------------------------------------------------------------------
local wio = {}

-----------------------------------------------------------------------------
-- Constants
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Persist data
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Private functions
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Public functions
-----------------------------------------------------------------------------

function wio.rewriteFile(filePath, content, createPath)
    createPath = createPath or true
    
    if createPath then
        local parentFolder = filesystem.path(filePath)
        if not filesystem.exists(parentFolder) then
            filesystem.makeDirectory(parentFolder)
        end
    end

    logf.line("DEBUG","wio",filePath)
    local file = io.open(filePath, "w")
    file:write(content)
    file:close()
    return true
end

function wio.appendToFile(filePath, content)
    local file = io.open(filePath, "a")
    file:write(content)
    file:close()
    return true
end

function wio.readFile(filePath)
    if not filesystem.exists(filePath) then
        return
    end
    
    local file = io.open(filePath, "r")
    local resultString = file:read()
    file:close()
    
    return resultString
end 

function wio.saveTableToFile(filePath, value, pretty)
    pretty = pretty or false
    wio.rewriteFile(filePath,serialization.serialize(value))
    return true
end

function wio.readTableFromFile(filePath)
    local valueString = wio.readFile(filePath)
    if valueString == nil then
        return
    end
    return serialization.unserialize(valueString)
end

return wio