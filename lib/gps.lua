local logf = require("logf")
local util = require("util")
local component = require("component")

local _broadcastStrength = 400

local modem = nil
if component.isAvailable("modem") then
  modem = component.modem
end
local computer = require("computer")
local floor, sqrt, abs = math.floor, math.sqrt, math.abs

local CHANNEL_GPS, gps = 65534, {}

local function round(v, m)
  m = m or 1.0
  return {
    x = floor((v.x+(m*0.5))/m)*m,
    y = floor((v.y+(m*0.5))/m)*m,
    z = floor((v.z+(m*0.5))/m)*m
  }
end

local function len(v)
  return sqrt(v.x^2 + v.y^2 + v.z^2)
end

local function cross(v, b)
  return {x = v.y*b.z-v.z*b.y, y = v.z*b.x-v.x*b.z, z = v.x*b.y-v.y*b.x}
end

local function dot(v, b)
  return v.x*b.x + v.y*b.y + v.z*b.z
end

local function add(v, b)
  return {x = v.x+b.x, y = v.y+b.y, z = v.z+b.z}
end

local function sub(v, b)
  return {x = v.x-b.x, y = v.y-b.y, z = v.z-b.z}
end

local function mul(v, m)
  return {x = v.x*m, y = v.y*m, z = v.z*m}
end

local function norm(v)
  return mul(v, 1/len(v))
end

local function trilaterate(A, B, C)
  local a2b = {x = B.x-A.x, y = B.y-A.y, z = B.z-A.z}
  local a2c = {x = C.x-A.x, y = C.y-A.y, z = C.z-A.z}
  if abs(dot(norm(a2b), norm(a2c))) > 0.999 then
    return nil
  end
  local d = len(a2b)
  local ex = norm(a2b)
  local i = dot(ex, a2c)
  local ey = norm(sub(a2c,mul(ex, i)))
  local j = dot(ey, a2c)
  local ez = cross(ex, ey)
  local r1 = A.d
  local r2 = B.d
  local r3 = C.d
  local x = (r1^2 - r2^2 + d^2) / (2*d)
  local y = (r1^2 - r3^2 - x^2 + (x-i)^2 + j^2) / (2*j)
  local result = add(A, add(mul(ex, x), mul(ey, y)))
  local zSquared = r1^2 - x^2 - y^2
  if zSquared > 0 then
    local z = sqrt( zSquared )
    local result1 = add(result, mul(ez, z))
    local result2 = sub(result, mul(ez, z))
    local rounded1, rounded2 = round(result1, 0.01), round(result2, 0.01)
    if rounded1.x ~= rounded2.x or rounded1.y ~= rounded2.y or rounded1.z ~= rounded2.z then
      return rounded1, rounded2
    else
      return rounded1
    end
  end
  return round(result, 0.01)
end

local function narrow(p1, p2, fix)
    local dist1 = abs(len(sub(p1, fix)) - fix.d)
    local dist2 = abs(len(sub(p2, fix)) - fix.d)
    if abs(dist1 - dist2) < 0.01 then
        return p1, p2
    elseif dist1 < dist2 then
        return round(p1, 0.01)
    else
        return round(p2, 0.01)
    end
end

local function isValueInTable(tab, value)
    for _,v in pairs(tab) do
        if v == value then
            return true
        end
    end
    return false
end

local function collectTransmitterData()
    modem.open(CHANNEL_GPS)
    local strength = modem.getStrength()
    modem.setStrength(_broadcastStrength)
    modem.broadcast(CHANNEL_GPS, "PING")
    logf.line("VERBOSE","gps","Collecting transmitter data")
    
    local adresses = {}
    local transmitters = {}
    local timeout = computer.uptime() + 5
    while true do
        local signal, _, sender, _, distance, message, x, y, z, name = computer.pullSignal(1)
        --logf.line("VERBOSE","gps","Signal:"..tostring(signal).." Arg 1:"..tostring(distance).." Arg 2:"..tostring(x).." Arg 3:"..tostring(y).." Arg 4:"..tostring(z).." Arg 6:"..tostring(name))
        
        if signal == "modem_message" and message == "GPS" then
            local alreadyAdded = isValueInTable(adresses, sender)
            if not alreadyAdded then
                local transmitter = {name = name, x = x, y = y, z = z, d = distance}
                if distance == 0 then
                    modem.setStrength(strength)
                    modem.close(CHANNEL_GPS)
                    logf.line("VERBOSE","gps","Detected point blank transmitter")
                    return {transmitter}
                end
                
                table.insert(transmitters, transmitter)
                table.insert(adresses, sender)
                logf.line("VERBOSE","gps","Transmitter "..tostring(name).." "..transmitter.d.." metres from "..transmitter.x..", "..transmitter.y..", "..transmitter.z)
            end
        end
        
        if #transmitters >= 4 then
            break
        end        
        if computer.uptime() >= timeout then
            break
        end
    end
    modem.setStrength(strength)
    modem.close(CHANNEL_GPS)
    return transmitters
end


local function locateInternal()
    local transmitters = collectTransmitterData()
    if #transmitters == 1 and transmitters[1].d == 0 then
        local transmitter = transmitters[1]
        return {transmitter.x, transmitter.y, transmitter.z}
    end
    
    if #transmitters < 3 then
        logf.line("WARNING","gps","Not enough transmitters detected")
        return nil
    end
    
    local aPoint = table.remove(transmitters)
    local bPoint = table.remove(transmitters)
    local cPoint = table.remove(transmitters)
    local pos1, pos2 = trilaterate(aPoint, bPoint, cPoint)
    
    if not pos1 then
        logf.line("WARNING","gps","Trilateration failed")
        return nil
    elseif pos1 and not pos2 then
        return pos1
    end

    if #transmitters == 0 then
        logf.line("WARNING","gps","No more transmitters. Impossible to narrow down position")
        return pos1, pos2
    end
    
    logf.line("WARNING","gps","Ambiguous position, trying to narrow")
    logf.line("WARNING","gps","Could be "..pos1.x..", "..pos1.y..", "..pos1.z.." or "..pos2.x..", "..pos2.y..", "..pos2.z)
        
    while pos1 and pos2 do
        if #transmitters == 0 then
            logf.line("WARNING","gps","Unable to narrow down position")
            return nil    
        end
    
        pos1, pos2 = narrow(pos1, pos2, table.remove(transmitters))
    end
    return pos1, pos2
end

function gps.locate(round)
    if round == nil then round = true end

    if not modem then
        logf.line("ERROR","gps","No wireless modem attached")
        return nil
    end
    logf.line("VERBOSE","gps","Finding position...")
    
    local pos1, pos2 = locateInternal()
    if pos1 and pos2 then
        logf.line("WARNING","gps","Ambiguous position")
        logf.line("WARNING","gps","Could be "..pos1.x..", "..pos1.y..", "..pos1.z.." or "..pos2.x..", "..pos2.y..", "..pos2.z)
        return nil
    elseif pos1 then
        --HACK. z incorrect for some reason by 1        
        if not round then
            logf.line("DEBUG","gps","Position is "..pos1.x..", "..pos1.y..", "..(pos1.z))
            return pos1.x, pos1.y, pos1.z
        end
        local x = util.round(pos1.x)
        local y = util.round(pos1.y)
        local z = util.round(pos1.z)
        
        logf.line("DEBUG","gps","Position is "..x..", "..y..", "..z)
        return x, y, z
    else
        logf.line("WARNING","gps","Could not determine position")
        return nil
    end
end

return gps