-----------------------------------------------------------------------------
-- Imports and dependencies
-----------------------------------------------------------------------------
local nav = require("nav")
local logf = require("logf")
local util = require("util")
local logf = require("logf")
local vector = require("vector")
local computer = require("computer")
local component = require("component")

-----------------------------------------------------------------------------
-- Module declaration
-----------------------------------------------------------------------------
local recharge = {}

-----------------------------------------------------------------------------
-- Constants
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Persist data
-----------------------------------------------------------------------------
local _autoRechargeEnabled = true
local _energyLowerLimit = 4000
local _energyUpperLimit = 10000

-----------------------------------------------------------------------------
-- Private functions
-----------------------------------------------------------------------------


-----------------------------------------------------------------------------
-- Public functions
-----------------------------------------------------------------------------
function recharge.isNeeded()
    if not _autoRechargeEnabled then
        return false
    end
    if not component.isAvailable("robot") then
        return false
    end
    return computer.energy() < _energyLowerLimit
end

function recharge.recharge()
    logf.line("DEBUG","recharge","Low energy: "..util.round(computer.energy()))
    logf.line("DEBUG","recharge","Recharging...")
    local x, y, z = nav.getPosition()
    local myCoordinates =  vector.new(x, y, z)
    local myFacing = nav.getFacing()

    logf.line("DEBUG","recharge","Navigating to recharger")
    local result, reason = nav.gisNavigate("charge")
    if not result then
        return false, "Navigation failed. Reason: "..reason
    end
    
    local requiredEnergy = math.min(computer.maxEnergy(),_energyUpperLimit)
    while computer.energy() < requiredEnergy do
        logf.line("DEBUG","recharge","Energy required: "..requiredEnergy..". Current energy: "..computer.energy())
        logf.line("DEBUG","recharge","Waiting for 2 seconds")
        os.sleep(2)
    end
    
    logf.line("DEBUG","recharge","Returning")
    local result, reason = nav.navigate(myCoordinates)
    if not result then
        return false, "Returning failed. Reason: "..reason
    end
    
    local result, reason = nav.faceSide(myFacing)
    if not result then
        return false, "Facing failed. Reason: "..reason
    end
    return true
end

return recharge