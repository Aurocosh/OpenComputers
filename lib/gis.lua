-----------------------------------------------------------------------------
-- Imports and dependencies
-----------------------------------------------------------------------------
local logf = require("logf")
local vector = require("vector")
local persist = require("persist")

-----------------------------------------------------------------------------
-- Module declaration
-----------------------------------------------------------------------------
local gis = {}

-----------------------------------------------------------------------------
-- Constants
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Persist data
-----------------------------------------------------------------------------
local _coordinates = {}

-----------------------------------------------------------------------------
-- Private functions
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Public functions
-----------------------------------------------------------------------------

function gis.addCoordinates(name, coordinates, unique)
    if unique == nil then unique = true end
    
    if unique or _coordinates[name] == nil then
        _coordinates[name] = {coordinates}
        return true
    end
        
    table.insert(_coordinates[name], coordinates)
    return true
end

function gis.deleteCoordinates(name)
    _coordinates[name] = nil
    return true
end

function gis.getCoordinates(name)
    local coordinatesList = _coordinates[name]
    if coordinatesList == nil then
        return nil
    end
    return coordinatesList[1]
end

function gis.getAllCoordinates()
    local locationsCopy = {}
    for k,v in pairs(_coordinates) do
        local coords = {}
        for i,c in pairs(v) do
            coords[i] = vector.new(c.x, c.y, c.z)
        end
        locationsCopy[k] = coords
    end
    return locationsCopy
end

function gis.clearCoordinates()
    _coordinates = {}
    return true
end


function gis.saveCoordinates()
    return persist.saveSharedData("coordinates", _coordinates)
end

function gis.loadCoordinates()
    _coordinates = persist.loadSharedData("coordinates")
    return true
end

return gis