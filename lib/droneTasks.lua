-----------------------------------------------------------------------------
-- Imports and dependencies
-----------------------------------------------------------------------------
local sides = require("sides")
local serialization = require("serialization")
local NavigationTaskClass = require("NavigationTaskClass")

-----------------------------------------------------------------------------
-- Module declaration
-----------------------------------------------------------------------------
local droneTasks = {}

-----------------------------------------------------------------------------
-- Constants
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Persist data
-----------------------------------------------------------------------------
local _currentTaskId
local _lastTaskId
local _tasks = {}

-----------------------------------------------------------------------------
-- Private functions
-----------------------------------------------------------------------------
local function getTaskClass(taskType)
    if taskType == "Navigate" then
        return NavigationTaskClass
    end
    return nil
end


local function performTask(task)
    
    
    --io.write("task.arguments "..serialization.serialize(task.arguments,true).."\n")
    
      
    local completed,errorMessage
    if task.arguments == nil then
        completed,errorMessage = task.action()
    else
        completed,errorMessage = task.action(table.unpack(task.arguments))
    end

    --io.write("x "..arguments[1].." z "..arguments[2].."\n")
    
    --local completed,errorMessage = pcall(task.action, arguments)    
    if completed or task.isVoid then
        table.remove(_tasks, 1);
    end
    return completed,errorMessage
end

local function removeCurrentTask()
    table.remove(_tasks, 1);
end

local function getTaskById(taskId)
    for _,v in pairs(_tasks) do
        if taskId == v.taskId then
            return v
        end
    end
    return nil
end

-----------------------------------------------------------------------------
-- Public functions
-----------------------------------------------------------------------------

function droneTasks.addTask(taskType, requestId, parameters)
    local taskClass = getTaskClass(taskType)
    local task = taskClass:create(taskId, requestId, parameters)
    table.insert(_tasks, 1, task);
end

function droneTasks.addDelayedTask(action, message, arguments)
    local taskClass = getTaskClass(taskType)
    local task = taskClass:create(taskId, requestId, parameters)
    table.insert(_tasks, task);
end

function droneTasks.clearAllTasks()
    _tasks = {}
    _currentTaskId = -1
end

function droneTasks.processAllTasks()
    local taskCount = #_tasks
    if taskCount == 0 then
        return
    end
    if taskCount ~= 0 and _currentTaskId == -1 and  then
        _currentTaskId = _tasks[1].taskId
    end
    if taskCount ~= 0 and _currentTaskId != _tasks[1].taskId then
        local task = getTaskById(_currentTaskId)
        task.SaveState()
        _currentTaskId = _tasks[1].taskId
    end
    
    local task = _tasks[1].taskId
    if 

    processNewTasks()
    processDelayedTasks()
    
    --io.write("_tasks"..serialization.serialize(_tasks,true).."\n")
    --io.write("task count "..#_tasks.."\n")
    
    while(#_tasks > 0) do 
        local task = _tasks[1]        
        announceTask(task)
        local completed,errorMessage = performTask(task)    
        if errorMessage ~= nil then
            io.write("Error occured: "..errorMessage.."\n")
            return
        end
        processNewTasks()
        processDelayedTasks()
    end
end

return tasks