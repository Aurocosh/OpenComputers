local vector = require("vector")

local box = {}
box.__index = box

function box:getA()
    return vector.new(self.cornerA.x, self.cornerA.y, self.cornerA.z)
end

function box:getB()
    return vector.new(self.cornerB.x, self.cornerB.y, self.cornerB.z)
end

function box:bottomNorthWest()
    return vector.new(self.cornerA.x, self.cornerA.y, self.cornerA.z)
end

function box:bottomNorthWest()
    return vector.new(self.cornerA.x, self.cornerA.y, self.cornerA.z)
end

function box:bottomNorthEast()
    return vector.new(self.cornerB.x, self.cornerA.y, self.cornerA.z)
end

function box:bottomSouthEast()
    return vector.new(self.cornerB.x, self.cornerA.y, self.cornerB.z)
end

function box:bottomSouthWest()
    return vector.new(self.cornerA.x, self.cornerA.y, self.cornerB.z)
end

function box:topNorthWest()
    return vector.new(self.cornerA.x, self.cornerB.y, self.cornerA.z)
end

function box:topNorthEast()
    return vector.new(self.cornerB.x, self.cornerB.y, self.cornerA.z)
end

function box:topSouthEast()
    return vector.new(self.cornerB.x, self.cornerB.y, self.cornerB.z)
end

function box:topSouthWest()
    return vector.new(self.cornerA.x, self.cornerB.y, self.cornerB.z)
end

function box:xLength()
    return self.cornerB.x - self.cornerA.x
end

function box:yLength()
    return self.cornerB.y - self.cornerA.y
end

function box:zLength()
    return self.cornerB.z - self.cornerA.z
end

function box:fixX()
    if self.cornerB.x < self.cornerA.x then
        local x = self.cornerA.x
        self.cornerA.x = self.cornerB.x
        self.cornerB.x = x
    end
end

function box:fixY()
    if self.cornerB.y < self.cornerA.y then
        local y = self.cornerA.y
        self.cornerA.y = self.cornerB.y
        self.cornerB.y = y
    end
end

function box:fixZ()
    if self.cornerB.z < self.cornerA.z then
        local z = self.cornerA.z
        self.cornerA.z = self.cornerB.z
        self.cornerB.z = z
    end
end

function box:addBottomX(value)
    self.cornerA.x = self.cornerA.x - value
    self:fixX()
end

function box:addBottomY(value)
    self.cornerA.y = self.cornerA.y - value
    self:fixY()
end

function box:addBottomZ(value)
    self.cornerA.z = self.cornerA.z - value
    self:fixZ()
end

function box:addTopX(value)
    self.cornerB.x = self.cornerB.x - value
    self:fixX()
end

function box:addTopY(value)
    self.cornerB.y = self.cornerB.y - value
    self:fixY()
end

function box:addTopZ(value)
    self.cornerB.z = self.cornerB.z - value
    self:fixZ()
end

function box:cloneDeep()
    return box.new(self:getA(),self:getB())
end

function box:__tostring()
    return self.cornerA.x..","..self.cornerA.y..","..self.cornerA.z.."|"..self.cornerB.x..","..self.cornerB.y..","..self.cornerB.z
end

function box.new(cornerA, cornerB)
    cornerA = assert(cornerA, "Invalid cornerA")
    cornerB = assert(cornerB, "Invalid cornerB")
    local c = {
		cornerA = cornerA,
        cornerB = cornerB
	}
	setmetatable(c, box)
    c:fixX()
    c:fixY()
    c:fixZ()
	return c
end

return box