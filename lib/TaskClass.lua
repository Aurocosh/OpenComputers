-----------------------------------------------------------------------------
-- Imports and dependencies
-----------------------------------------------------------------------------
local logf = require("logf")

-----------------------------------------------------------------------------
-- Class declaration
-----------------------------------------------------------------------------
local TaskClass = {}
TaskClass.__index = TaskClass

-----------------------------------------------------------------------------
-- Constants
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Persist data
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Private functions
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Public functions
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Member functions
-----------------------------------------------------------------------------

function TaskClass:New(command)
   local instance = {}  -- our new object
   setmetatable(instance,TaskClass) -- make TaskClass handle lookup
   instance.taskID = taskID
   instance.requestId = requestId
   instance.x = parameters.x
   instance.y = parameters.y
   instance.z = parameters.z
   instance.isPaused = false
   instance.path = {}
   instance:RecalculatePath()
   return instance
end

function TaskClass:RecalculatePath()
    self.path = {}
    x,y,z = nav.getPosition()
    local xMovement = x - self.x
    if math.abs(xMovement) > 0 then
        local pathPart = {}
        pathPart.direction = xMovement > 0 and sides.posx or sides.negx
        pathPart.distance = math.abs(xMovement)
        table.insert(self.path, pathPart)
    end
    
    local yMovement = y - self.y
    if math.abs(yMovement) > 0 then
        local pathPart = {}
        pathPart.direction = yMovement > 0 and sides.posy or sides.negy
        pathPart.distance = math.abs(yMovement)
        table.insert(self.path, pathPart)
    end
    
    local zMovement = z - self.z
    if math.abs(zMovement) > 0 then
        local pathPart = {}
        pathPart.direction = zMovement > 0 and sides.posz or sides.negz
        pathPart.distance = math.abs(zMovement)
        table.insert(self.path, pathPart)
    end
end

function TaskClass:DoTaskStep()
    if #self.path == 0 then
        return true
    end
    local currentMove = self.path[1]
    
    local ok = nav.step(currentMove.direction)
    if ok then
        currentMove.count = currentMove.count - 1
    end
    if currentMove.count <= 0 then
        table.remove(self.path,1)
    end
    return false
end

function TaskClass:SaveState()
    self.isPaused = true
end

function TaskClass:RestoreState()
    self:RecalculatePath()
    self.isPaused = false
end

return TaskClass