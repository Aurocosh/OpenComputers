-----------------------------------------------------------------------------
-- Imports and dependencies
-----------------------------------------------------------------------------
local logf = require("logf")
local sides = require("sides")
local vector = require("vector")

-----------------------------------------------------------------------------
-- Module declaration
-----------------------------------------------------------------------------
local pathConstruct = {}

-----------------------------------------------------------------------------
-- Constants
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Persist data
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Private functions
-----------------------------------------------------------------------------


local function getMovementX(distance) 
    local movement = {}
    movement.direction = distance >= 0 and sides.posx or sides.negx
    movement.distance = math.abs(distance)
    return movement
end

local function getMovementY(distance) 
    local movement = {}
    movement.direction = distance >= 0 and sides.posy or sides.negy
    movement.distance = math.abs(distance)
    return movement
end

local function getMovementZ(distance) 
    local movement = {}
    movement.direction = distance >= 0 and sides.posz or sides.negz
    movement.distance = math.abs(distance)
    return movement
end

-----------------------------------------------------------------------------
-- Public functions
-----------------------------------------------------------------------------

function pathConstruct.constructDirectPath(fromPosition, toPosition, fromFacing, toFacing) 
    logf.line("INFO","Path","Calculating direct path")
    local path = {}
    if fromPosition.y ~= toPosition.y then
        local movementY = getMovementY(toPosition.y - fromPosition.y)
        table.insert(path, movementY)
    end
    
    local movementX = getMovementX(toPosition.x - fromPosition.x)
    local movementZ = getMovementZ(toPosition.z - fromPosition.z)
    
    local zFacing = fromPosition.z < toPosition.z and sides.posz or sides.negz
    if zFacing == fromFacing then
        table.insert(path, movementZ)
        table.insert(path, movementX)
    else
        table.insert(path, movementX)
        table.insert(path, movementZ)
    end
    
    return path
end

return pathConstruct