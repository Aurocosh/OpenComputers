-----------------------------------------------------------------------------
-- Imports and dependencies
-----------------------------------------------------------------------------
local box = require("box")
local logf = require("logf")
local vector = require("vector")
local persist = require("persist")

-----------------------------------------------------------------------------
-- Module declaration
-----------------------------------------------------------------------------
local rgis = {}

-----------------------------------------------------------------------------
-- Constants
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Persist data
-----------------------------------------------------------------------------
local _regions = {}

-----------------------------------------------------------------------------
-- Private functions
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Public functions
-----------------------------------------------------------------------------

function rgis.addRegion(name, region, unique)
    if unique == nil then unique = true end
    
    if unique or _regions[name] == nil then
        _regions[name] = {region}
        return true
    end
        
    table.insert(_regions[name], region)
    return true
end

function rgis.deleteRegion(name)
    _regions[name] = nil
    return true
end

function rgis.getRegion(name)
    local regionList = _regions[name]
    if regionList == nil then
        return nil
    end
    return regionList[1]
end

function rgis.getAllRegions()
    local regionsCopy = {}
    for k,v in pairs(_regions) do
        local regs = {}
        for i,b in pairs(v) do
            local cornerA = b.cornerA:cloneDeep()
            local cornerB = b.cornerB:cloneDeep()
            regs[i] = box.new(cornerA, cornerB)
        end
        regionsCopy[k] = regs
    end
    return regionsCopy
end

function rgis.clearRegions()
    _regions = {}
    return true
end


function rgis.saveRegions()
    return persist.saveSharedData("regions", _regions)
end

function rgis.loadRegions()
    _regions = persist.loadSharedData("regions")
    for k,r in pairs(_regions) do
        for _,b in pairs(r) do
            setmetatable(b, box)
            b:fixX()
            b:fixY()
            b:fixZ()
            setmetatable(b.cornerA, vector)
            setmetatable(b.cornerB, vector)
        end
    end
    return true
end

return rgis