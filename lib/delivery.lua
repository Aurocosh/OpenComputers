-----------------------------------------------------------------------------
-- Imports and dependencies
-----------------------------------------------------------------------------
local nav = require("nav")
local logf = require("logf")
local util = require("util")
local logf = require("logf")
local robot = require("robot")
local vector = require("vector")
local computer = require("computer")
local component = require("component")

-----------------------------------------------------------------------------
-- Module declaration
-----------------------------------------------------------------------------
local delivery = {}

-----------------------------------------------------------------------------
-- Constants
-----------------------------------------------------------------------------
local _dropWait = 2

-----------------------------------------------------------------------------
-- Persist data
-----------------------------------------------------------------------------
local _autoDeliverEnabled = true
local _slots = {1,2,3,4,5,6,7,8,9,10,11,12}
local _freeSlotsRequired = 2


if not component.isAvailable("robot") then
    logf("ERROR","delivery","Only robots can deliver items")
    return false
end
local drone = component.robot

-----------------------------------------------------------------------------
-- Private functions
-----------------------------------------------------------------------------
local function dropItems()
    logf.line("INFO","delivery","Dropping off items")
    for _,slot in pairs(_slots) do
        if robot.count(slot) > 0 then
            robot.select(slot)
            local wait = 1
            repeat
                if not robot.dropDown() then
                    os.sleep(wait)
                    wait = math.min(10, wait + 1)
                end
            until robot.count(slot) == 0
        end
    end
    robot.select(1)
    return true
end

-----------------------------------------------------------------------------
-- Public functions
-----------------------------------------------------------------------------
function delivery.isNeeded()
    if not _autoDeliverEnabled then
        return false
    end
    local emptyCount = 0;
    for _,slot in pairs(_slots) do
        if robot.count(slot) == 0 then
            emptyCount = emptyCount + 1
        end
    end
    return emptyCount < _freeSlotsRequired
end

function delivery.deliver()
    logf.line("INFO","delivery","Item delivery required")
    logf.line("INFO","delivery","Delivering...")
    local x, y, z = nav.getPosition()
    local myCoordinates =  vector.new(x, y, z)
    local myFacing = nav.getFacing()

    logf.line("DEBUG","delivery","Navigating to recharger")
    local result, reason = nav.gisNavigate("dropoff")
    if not result then
        return false, "Navigation failed. Reason: "..reason
    end
    
    dropItems()
    
    logf.line("DEBUG","delivery","Returning")
    local result, reason = nav.navigate(myCoordinates)
    if not result then
        return false, "Returning failed. Reason: "..reason
    end
    
    local result, reason = nav.faceSide(myFacing)
    if not result then
        return false, "Facing failed. Reason: "..reason
    end
    return true
end

return delivery