-----------------------------------------------------------------------------
-- Imports and dependencies
-----------------------------------------------------------------------------
local nav = require("nav")
local box = require("box")
local rgis = require("rgis")
local logf = require("logf")
local util = require("util")
local robot = require("robot")
local sides = require("sides")
local miner = require("miner")
local vector = require("vector")
local component = require("component")

-----------------------------------------------------------------------------
-- Module declaration
-----------------------------------------------------------------------------
local excavate = {}

-----------------------------------------------------------------------------
-- Constants
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Persist data
-----------------------------------------------------------------------------
local robotComponent = component.robot

-----------------------------------------------------------------------------
-- Private functions
-----------------------------------------------------------------------------

local function digForward(length)
    logf.line("DEBUG","excavate","Digging line. Length "..length)   
    for i = 1, length do
        miner.step()
    end
    coroutine.yield()
    return true
end

local function digLayer(width, heigth)
    logf.line("DEBUG","excavate","Digging layer")
    for i = 1, width do
        digForward(heigth-1)
        logf.line("DEBUG","excavate","Making a turn")
        if i ~= width then 
            if i%2 == 1 then
                nav.turnRight()
                miner.step()
                nav.turnRight()
            else
                nav.turnLeft()
                miner.step()
                nav.turnLeft()
            end
        end
    end
    return false
end

-----------------------------------------------------------------------------
-- Public functions
-----------------------------------------------------------------------------

function excavate.rgisDig(regionName) 
    local region = rgis.getRegion(regionName)
    if region == nil then
        logf.line("WARNING","excavate","Unknown region: "..regionName)
        return false, "Unknown region: "..regionName
    end
    return excavate.dig(region)
end

function excavate.dig(box)
    nav.setDiggingAllowed(true)
    
    box = box:cloneDeep()
    local extraY = 3 - box:yLength() % 3
    logf.line("WARNING","excavate","Extra y: "..extraY)
    box:addTopY(extraY)
    
    local quarryOrigin =  box:topNorthWest() - vector.new(0, -1, 0)
    logf.line("WARNING","excavate","quarryOrigin: "..tostring(quarryOrigin))
    logf.line("WARNING","excavate","topNorthWest: "..tostring(box:topNorthWest() ))
    logf.line("WARNING","excavate","bottomNorthWest: "..tostring(box:bottomNorthWest() ))
    local myFacing = sides.east
    
    local layerShift =  vector.new(0, -3, 0)
    local layerCount = math.ceil(box:yLength() / 3)
    local quarryWidth = box:xLength()
    local quarryHeigth = box:zLength()

    logf.line("INFO","excavate","Starting excavation")
    local currentLayer = 1
    while currentLayer <= layerCount do
        local start = quarryOrigin + (layerShift * (currentLayer - 1))
        logf.line("INFO","excavate","Moving to position "..tostring(start))
        nav.navigate(start)
        nav.faceSide(myFacing)
        digLayer(quarryWidth, quarryHeigth)
        currentLayer = currentLayer + 1
    end
end

return excavate