-----------------------------------------------------------------------------
-- Imports and dependencies
-----------------------------------------------------------------------------
local term = require("term")

-----------------------------------------------------------------------------
-- Module declaration
-----------------------------------------------------------------------------
local util = {}

-----------------------------------------------------------------------------
-- Constants
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Persist data
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Private functions
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Public functions
-----------------------------------------------------------------------------

function util.round(value)
    return math.floor(value + 0.5)
end

function util.boolToNumber(value)
    return value and 1 or 0
end

function util.toBool(value)
    return not not value
end

function util.copyShallow(value)
    if type(value) ~= "table" then return value end
    local meta = getmetatable(value)
    local target = {}
    for k, v in pairs(value) do target[k] = v end
    setmetatable(target, meta)
    return target
end

return util