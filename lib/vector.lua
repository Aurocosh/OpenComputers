local vector = {}
vector.__index = vector

function vector:add(other)
    return vector.new(
        self.x + other.x,
        self.y + other.y,
        self.z + other.z
    )
end

function vector:sub(other)
    return vector.new(
        self.x - other.x,
        self.y - other.y,
        self.z - other.z
    )
end

function vector:mul(m)
    return vector.new(
        self.x * m,
        self.y * m,
        self.z * m
    )
end

function vector:neg()
    return vector.new(
        -self.x,
        -self.y,
        -self.z
    )
end

function vector:div(m)
    return vector.new(
        self.x / m,
        self.y / m,
        self.z / m
    )
end

function vector:dot(other)
    return self.x*other.x + self.y*other.y + self.z*other.z
end

function vector:cross(other)
    return vector.new(
        self.y*other.z - self.z*other.y,
        self.z*other.x - self.x*other.z,
        self.x*other.y - self.y*other.x
    )
end

function vector:length()
    return math.sqrt( self.x*self.x + self.y*self.y + self.z*self.z)
end

function vector:equals(other)
    return self.x == other.x and self.y == other.y and self.z == other.z
end

function vector:normalize()
    return self:mul(1 / self:length())
end

function vector:round(nTolerance)
    nTolerance = nTolerance or 1.0
    return vector.new(
        math.floor( (self.x + (nTolerance * 0.5)) / nTolerance ) * nTolerance,
        math.floor( (self.y + (nTolerance * 0.5)) / nTolerance ) * nTolerance,
        math.floor( (self.z + (nTolerance * 0.5)) / nTolerance ) * nTolerance
    )
end

function vector:cloneDeep()
    return vector.new(self.x, self.y, self.z)
end

function vector:__tostring()
    return self.x..","..self.y..","..self.z
end

vector.__add = vector.add
vector.__sub = vector.sub
vector.__mul = vector.mul
vector.__unm = vector.neg
vector.__div = vector.div
vector.__len = vector.length
vector.__eq = vector.equals

function vector.new(x, y, z)
	local v = {
		x = x or 0,
		y = y or 0,
		z = z or 0
	}
	setmetatable(v, vector)
	return v
end

return vector