-----------------------------------------------------------------------------
-- Imports and dependencies
-----------------------------------------------------------------------------
local shell = require("shell")

-----------------------------------------------------------------------------
-- Module declaration
-----------------------------------------------------------------------------
local wshell = {}

-----------------------------------------------------------------------------
-- Constants
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Persist data
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Private functions
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Public functions
-----------------------------------------------------------------------------
function wshell.execute(command, taskData)
    local taskEnv = _G
    local dataBackup = taskEnv.taskData
    taskEnv.taskData = taskData
    
    local result,err = shell.execute(command,taskEnv)
    if not result then
        return false, err
    end
    
    local interrupted = taskEnv.interrupted
    local internalData = taskEnv.taskData
    taskEnv.interrupted = nil
    taskEnv.taskData = dataBackup
    return true, nil, interrupted, internalData
end

return wshell