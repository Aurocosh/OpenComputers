-----------------------------------------------------------------------------
-- Imports and dependencies
-----------------------------------------------------------------------------
local gps = require("gps")
local nav = require("nav")
local logf = require("logf")
local robot = require("robot")
local sides = require("sides")
local component = require("component")

-----------------------------------------------------------------------------
-- Module declaration
-----------------------------------------------------------------------------
local pflocate = {}

-----------------------------------------------------------------------------
-- Constants
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Persist data
-----------------------------------------------------------------------------
local _locationAttempts = 5
local _newLocationSleep = 2

-----------------------------------------------------------------------------
-- Private functions
-----------------------------------------------------------------------------
local function determineFacing(x, z, nx, nz)
    if x == nx and z == nz then
        return false, "Old position equial to new position"
    end
    if math.abs(nx-x) > 1 then
        return false, "X coordinate difference is too big"
    end
    if math.abs(nz-z) > 1 then
        return false, "Z coordinate difference is too big"
    end
    if math.abs(nx-x) + math.abs(nz-z) == 2 then
        return false, "Two coordinates changed"
    end
    
    if nx-x == 1 then
        return true, nil, sides.east
    elseif nx-x == -1 then
        return true, nil, sides.west
    elseif nz-z == 1 then
        return true, nil, sides.south
    elseif nz-z == -1 then
        return true, nil, sides.north        
    end
    
    return false, "Unknown error"
end

local function tryToLocate()
    local x, y, z = gps.locate()
    if x == nil then
        return false, "Unable to receive gps data"
    end
    
    for i = 1,3 do
        local detected = robot.detect(sides.front)
        if detected then
            nav.turnRight()
        end
    end
    
    result = nav.forward()
    if not result then
       return false, "Unable to move"
    end
    
    logf.line("DEBUG", "pflocate", "Sleeping for ".._newLocationSleep.." seconds")
    os.sleep(_newLocationSleep)
    
    local nx, ny, nz = gps.locate()
    if nx == nil then
        nav.back()
        return false, "Unable to receive new gps data"
    end
    
    local result, err, facing = determineFacing(x, z, nx, nz)
    if not result then 
        nav.back()
        return false, err
    end
    
    nav.back()
    return true, nil, x, y, z, facing
end

-----------------------------------------------------------------------------
-- Public functions
-----------------------------------------------------------------------------
function pflocate.locate()
    if not component.isAvailable("robot") then
        logf.line("ERROR", "pflocate", "Position and facing detection only available for robots")
        return false
    end
    
    logf.line("INFO","pflocate","Starting position and facing detection")
    for i = 1,_locationAttempts do
        logf.line("INFO","pflocate","Attempt "..i)
        local result, err, x, y, z, facing = tryToLocate()
        if result then
            return true, nil, x, y, z, facing
        else
            logf.line("INFO","pflocate","Attempt "..i.." failed. Reason: "..err)        
        end        
    end
    
    return false, "All detection attempts failed"
end

function pflocate.update()
    local result, err, x, y, z, facing = pflocate.locate()
    if not result then
        return result, err
    end
    
    nav.setPosition(x, y, z, facing)
    return true
end

return pflocate