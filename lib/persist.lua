-----------------------------------------------------------------------------
-- Imports and dependencies
-----------------------------------------------------------------------------
local wio = require("wio")
local process = require("process")
local filesystem = require("filesystem")
local serialization = require("serialization")

-----------------------------------------------------------------------------
-- Module declaration
-----------------------------------------------------------------------------
local persist = {}

-----------------------------------------------------------------------------
-- Constants
-----------------------------------------------------------------------------
persist.Executed = 1
persist.Returned = 2
persist.Error = 3
persist.Interrupted = 4

-----------------------------------------------------------------------------
-- Persist data
-----------------------------------------------------------------------------
local prettyResult = true
local consumeResult = true

local prettyState = true
local consumeState = true

local prettyData = false
local consumeData = false

local dataDirectory = "/data"
local resultPath = "/data/result"

-----------------------------------------------------------------------------
-- Private functions
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Public functions
-----------------------------------------------------------------------------

function persist.saveResult(code, data)
    local result = {}
    result.code = code
    result.data = data
    filesystem.makeDirectory(dataDirectory)
    return wio.saveTableToFile(resultPath,result,prettyResult)
end

function persist.loadResult(consume)
    if not filesystem.exists(resultPath) then
        return 1
    end
    local result = wio.readTableFromFile(resultPath)
    
    if consume == nil then consume = consumeResult end
    if consume then
        filesystem.remove(resultPath)
    end
    
    return result.code, result.data
end

function persist.saveState(state)
    local command = process.info().command
    local stateDirectory = dataDirectory.."/"..command
    local statePath = stateDirectory.."/state"
    return wio.saveTableToFile(statePath,state,prettyState)
end

function persist.loadState(consume)
    local command = process.info().command
    local statePath = dataDirectory.."/"..command.."/state"
    if not filesystem.exists(statePath) then
        return
    end
    
    local state = wio.readTableFromFile(statePath)
    
    if consume == nil then consume = consumeState end
    if consume then
        filesystem.remove(statePath)
    end
    
    return state
end

function persist.saveData(name, data)
    local command = process.info().command
    local programDirectory = dataDirectory.."/"..command
    local dataPath = programDirectory.."/"..name
    return wio.saveTableToFile(dataPath, data, prettyData)
end

function persist.loadData(name, consume)
    local command = process.info().command
    local dataPath = dataDirectory.."/"..command.."/"..name

    if not filesystem.exists(dataPath) then
        return
    end
    
    local data = wio.readTableFromFile(dataPath)
    
    if consume == nil then consume = consumeData end
    if consume then
        filesystem.remove(dataPath)
    end
    
    return data
end

function persist.saveSharedData(name, data)
    local dataPath = dataDirectory.."/"..name
    return wio.saveTableToFile(dataPath, data, prettyData)
end

function persist.loadSharedData(name, consume)
    local dataPath = dataDirectory.."/"..name

    if not filesystem.exists(dataPath) then
        return
    end
    
    local data = wio.readTableFromFile(dataPath)
    
    if consume == nil then consume = consumeData end
    if consume then
        filesystem.remove(dataPath)
    end
    
    return data
end

return persist