
local wio = require("wio")
local wterm = require("wterm")
local process = require("process")
local persist = require("persist")
local serialization = require("serialization")

local testTable = {}
testTable.name = "Tom"
testTable.numbers = {1,2,34,4,5}
testTable.int = 6
testTable.bool = true

persist.saveResult(persist.Returned,{tab = {1,233,414,2},test="asd"})
local code,data = persist.loadResult()

wterm.write("Result test")
wterm.write("code "..code)
wterm.writeTable(data,false)
wterm.newLine()

persist.saveState({tab = {1,233,414,2},test="asd"})
local data = persist.loadState()
wterm.write("State test")
wterm.writeTable(data,false)
wterm.newLine()

persist.saveData("personal", {data = {2,2,3,4},test="test"})
local data = persist.loadData("personal")
wterm.write("Personal data test")
wterm.writeTable(data,false)
wterm.newLine()

persist.saveSharedData("shared", {data = {2,2,3,5},test="test2"})
local data = persist.loadSharedData("shared")
wterm.write("Shared data test")
wterm.writeTable(data,false)
wterm.newLine()

--term.write("ret info "..serialization.serialize(process.info().command,true).."\n")

