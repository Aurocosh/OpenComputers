local shell = require("shell")
local wterm = require("wterm")

local args, options = shell.parse(...)
if #args < 1 then
    wterm.write("Usage: unload <all|name>")
    return false
end

local name = args[1]
if not name then
    wterm.writeError("invalid library name")
    return false
end

if name == "all" then
    package.loaded.wio = nil
    package.loaded.nav = nil
    package.loaded.gps = nil
    package.loaded.gis = nil
    package.loaded.logf = nil
    package.loaded.plan = nil
    package.loaded.util = nil
    package.loaded.wterm = nil
    package.loaded.tasks = nil
    package.loaded.wshell = nil
    package.loaded.wsides = nil
    package.loaded.persist = nil
    package.loaded.recharge = nil
    package.loaded.pflocate = nil
    package.loaded.pathConstruct = nil
    wterm.write("Unloaded all custom libs")
else
    package.loaded[name] = nil
    wterm.write("Unloaded library: "..name)
end
