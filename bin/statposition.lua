local gps = require("gps")
local logf = require("logf")
local util = require("util")
local shell = require("shell")
local wterm = require("wterm")
local component = require("component")

local args, options = shell.parse(...)
if not options.i and not options.u and not options.s then
    wterm.write("Usage: statposition [-i|-u|-s] [x] [y] [z]")
    wterm.write(" -i: print current position.")
    wterm.write(" -u: update current position.")
    return false
end

if util.boolToNumber(options.u) + util.boolToNumber(options.i) + util.boolToNumber(options.s) > 1 then
    logf.line("ERROR", "position", "Passed several mutually exclusive flags")
    return false
end

if options.i then
    local x, y, z = gps.locate()
    if x == nil then
        logf.line("WARNING", "statposition", "Failed to determine position")
        return true    
    end
    wterm.write("Position: x:"..x.." y:"..y.." z:"..z)
    return true
end

if options.u then

    return true
end

return true