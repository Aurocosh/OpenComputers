local gis = require("gis")
local logf = require("logf")
local util = require("util")
local shell = require("shell")
local wterm = require("wterm")

local args, options = shell.parse(...)
if not options.s and not options.l then
    wterm.write("Usage: gis [-s|-l]")
    wterm.write(" -s: save coordinates to disk.")
    wterm.write(" -l: load coordinates from disk.")
    return false
end

if util.boolToNumber(options.s) + util.boolToNumber(options.l) > 1 then
    logf.line("ERROR", "gis", "Passed several mutually exclusive flags")
    return false
end

if options.s then
    gis.saveCoordinates()
    wterm.write("Saved coordinates to disk")
else
    gis.loadCoordinates()
    wterm.write("Loaded coordinates from disk")
end

return true