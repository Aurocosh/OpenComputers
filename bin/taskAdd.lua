local wterm = require("wterm")
local shell = require("shell")
local tasks = require("tasks")

local args, options = shell.parse(...)
if #args == 0 then
  wterm.write("Usage: taskAdd [-i] <command>")
  wterm.write(" -i: perform task immediately.")
  return
end

local command = args[1]

tasks.addTask(command, not options.i)
wterm.write("New task added: "..command)
