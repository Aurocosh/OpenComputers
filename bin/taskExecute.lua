local wterm = require("wterm")
local shell = require("shell")
local tasks = require("tasks")
local event = require("event")
local recharge = require("recharge")
local delivery = require("delivery")

local args, options = shell.parse(...)
if not options.a and not options.o then
  wterm.write("Usage: taskExecute [-o|-a]")
  wterm.write(" -o: perform one task.")
  wterm.write(" -a: perform all tasks.")
  return
end
if options.a and options.o then
    wterm.write("Invalid options")
    return false
end

local mainCoroutine
local interfaceCoroutine
local maintenaceCoroutine

local running = true
local char_s = string.byte("s")
local char_r = string.byte("r")
local char_c = string.byte("c")
local char_q = string.byte("q")

function unknownEvent()
end

local myEventHandlers = setmetatable({}, { __index = function() return unknownEvent end })

function myEventHandlers.key_up(adress, char, code, playerName)
    if char == char_q then
        wterm.write("Closing")
        running = false
    elseif char == char_s then
        wterm.write("Stopping main thread")
        mainThread:suspend()
    elseif char == char_r then
        wterm.write("Resuming main thread")
        mainThread:resume()
    elseif char == char_c then
        term.clear()
    end
end

function myEventHandlers.interrupted()
    wterm.write("Soft interrupt, closing")
    running = false
end

function myEventHandlers.modem_message(receiver, sender, port, distance, arg1, arg2, arg3, arg4, arg5)
    arg1 = tostring(arg1)
    arg2 = tostring(arg2)
    arg3 = tostring(arg3)
    arg4 = tostring(arg4)
    arg5 = tostring(arg5)

    logf.line("INFO","netlogger","Sender: "..sender.." Port: "..port.." Distance:"..distance.." Arg1:"..arg1.." Arg2:"..arg2.." Arg3:"..arg3.." Arg4:"..arg4.." arg5:"..arg5)
end

-- The main event handler as function to separate eventID from the remaining arguments
function handleEvent(eventID, ...)
    if (eventID) then -- can be nil if no event was pulled for some time
        myEventHandlers[eventID](...) -- call the appropriate event handler with all remaining arguments
    end
end

local function main()
    wterm.write("Main coroutine started")
    
    local tasksPerformed = 0
    while tasks.taskCount() > 0 and (options.a or tasksPerformed == 0 and options.o) do
        local taskCount = tasks.taskCount()
        if taskCount == 0 then
            wterm.write("No more tasks to execute")
            return true
        end
        
        local taskHandler, taskParameters = tasks.getNextTask()
        if not taskHandler then
            wterm.writeError("No more tasks to execute")
            return false
        end
        
        local taskCoroutine = coroutine.create(taskHandler)
        --wterm.writeTable(taskParameters)
        local result, reason = coroutine.resume(taskCoroutine, table.unpack(taskParameters))
        while coroutine.status(taskCoroutine) == "suspended" do 
            result, reason = coroutine.resume(taskCoroutine)
            coroutine.yield()
        end
        if not result then
            wterm.writeError("Main coroutine encountered error. Error "..reason)
            return false
        end
        
        local newTaskCount = tasks.newTaskCount()
        if newTaskCount > 0 then
            wterm.write(newTaskCount.." new tasks added")
        end

        local delayedTaskCount = tasks.delayedTaskCount()
        if delayedTaskCount > 0 then
            wterm.write(delayedTaskCount.." delayed tasks added")
        end
        tasksPerformed = tasksPerformed + 1
    end
end

local function interface()
    wterm.write("Interface thread started\n")
    -- main event loop which processes all events, or sleeps if there is nothing to do
    while running do
        handleEvent(event.pull()) -- sleeps until an event is available, then process it
    end
end

local function maintenance()
    wterm.write("Maintenance coroutine started")
    while true do
        wterm.write("Performing maintenance")
        if recharge.isNeeded() then
            local result, reason = recharge.recharge()
            if not result then
                wterm.writeError("Maintenance failed while recharging. "..reason)
                return false
            end
        end
        if delivery.isNeeded() then
            local result, reason = delivery.deliver()
            if not result then
                wterm.writeError("Maintenance failed while delivering. "..reason)
                return false
            end
        end
        coroutine.yield()
    end
end

mainCoroutine = coroutine.create(main)
--interfaceCoroutine = coroutine.create(interface)
maintenaceCoroutine = coroutine.create(maintenance)

while coroutine.status(mainCoroutine) == "suspended" and coroutine.status(maintenaceCoroutine) == "suspended" do 
    coroutine.resume(mainCoroutine)
    coroutine.resume(maintenaceCoroutine)
end

return true