local nav = require("nav")
local logf = require("logf")
local shell = require("shell")
local wterm = require("wterm")
local wsides = require("wsides")
local vector = require("vector")
local component = require("component")
local pathConstruct = require("pathConstruct")

if not component.isAvailable("robot") then
    wterm.writeError("Can only run on robots")
    return false
end

local args, options = shell.parse(...)
if #args < 1 then
    wterm.write("Usage: navigate [-b] [-d] -- <x> <y> <z>")
    wterm.write(" -b: break blocks.")
    return false
end

local x = tonumber(args[1])
if not x then
    wterm.writeError("invalid x coordinate")
    return false
end

local y = tonumber(args[2])
if not y then
    wterm.writeError("invalid y coordinate")
    return false
end

local z = tonumber(args[3])
if not z then
    wterm.writeError("invalid z coordinate")
    return false
end

nav.setDiggingAllowed(options.b)

local target = vector.new(x, y, z)
local navigationCoroutine = coroutine.create(nav.navigate)
local result, reason = coroutine.resume(navigationCoroutine, target)
while coroutine.status(navigationCoroutine) == "suspended" do
    result, reason = coroutine.resume(navigationCoroutine)
end

if not result then
    wterm.writeError("Navigation failed. Reason: "..reason)
    return false
end

return true