local wterm = require("wterm")
local shell = require("shell")
local tasks = require("tasks")

--[[

local args, options = shell.parse(...)
if #args == 0 then
  wterm.write("Usage: taskList [-i] <command>")
  wterm.write(" -i: perform task immediately.")
  return
end
local command = args[1]
--]]

local queued, new, delayed = tasks.getTaskList()
if #new > 0 then
    wterm.write("New tasks:")
    for k,v in pairs(new) do 
        local command = v.command
        local isNew = v.taskData == nil
        local status = isNew and "New" or "Interrupted"
        wterm.write(" "..k..")"..' Command: "'..command..'". Status: '..status)
    end
    wterm.newLine()
end

if #queued > 0 then
    wterm.write("Queued tasks:")
    for k,v in pairs(queued) do 
        local command = v.command
        local isNew = v.taskData == nil
        local status = isNew and "New" or "Interrupted"
        wterm.write(" "..k..")"..' Command: "'..command..'". Status: '..status)
    end
    wterm.newLine()
end

if #delayed > 0 then
    wterm.write("Delayed tasks:")
    for k,v in pairs(delayed) do 
        local command = v.command
        local isNew = v.taskData == nil
        local status = isNew and "New" or "Interrupted"
        wterm.write(" "..k..")"..' Command: "'..command..'". Status: '..status)
    end
    wterm.newLine()
end
