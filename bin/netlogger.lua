local logf = require("logf")
local util = require("util")
local term = require("term")
local event = require("event")
local shell = require("shell")
local wterm = require("wterm")
local component = require("component")

if not component.isAvailable("modem") then
    logf.line("ERROR", "position", "Position can only autoupdate on robots")
    return false
end

local args, options = shell.parse(...)
if #args ~= 1 then
    wterm.write("Usage: netlogger <port>")
    return false
end

local port = tonumber(args[1])
if not port or port <= 0 or port >= 65535 then
    wterm.writeError("invalid port")
    return
end

local running = true -- state variable so the loop can terminate
local modem = component.modem
local char_g = string.byte("g")
local char_c = string.byte("c")
local char_q = string.byte("q")

function unknownEvent()
  -- do nothing if the event wasn't relevant
end

local myEventHandlers = setmetatable({}, { __index = function() return unknownEvent end })
 
-- table that holds all event handlers
-- in case no match can be found returns the dummy function unknownEvent
 
-- Example key-handler that simply sets running to false if the user hits space
function myEventHandlers.key_up(adress, char, code, playerName)
    if char == char_q then
        wterm.write("Closing")
        running = false
    elseif char == char_g then
        wterm.write("Pinging port "..port)
        modem.broadcast(port, "PING")
    elseif char == char_c then
        term.clear()
    end
end

function myEventHandlers.interrupted()
    wterm.write("Soft interrupt, closing")
    running = false
end

function myEventHandlers.modem_message(receiver, sender, port, distance, arg1, arg2, arg3, arg4, arg5)
    arg1 = tostring(arg1)
    arg2 = tostring(arg2)
    arg3 = tostring(arg3)
    arg4 = tostring(arg4)
    arg5 = tostring(arg5)

    logf.line("INFO","netlogger","Sender: "..sender.." Port: "..port.." Distance:"..distance.." Arg1:"..arg1.." Arg2:"..arg2.." Arg3:"..arg3.." Arg4:"..arg4.." arg5:"..arg5)
end
-- The main event handler as function to separate eventID from the remaining arguments
function handleEvent(eventID, ...)
    if (eventID) then -- can be nil if no event was pulled for some time
        myEventHandlers[eventID](...) -- call the appropriate event handler with all remaining arguments
    end
end
 
modem.open(port)
logf.line("INFO","gps","Started netlogger on port "..port)
 
-- main event loop which processes all events, or sleeps if there is nothing to do
while running do
    handleEvent(event.pull()) -- sleeps until an event is available, then process it
end

modem.close(port)

return true