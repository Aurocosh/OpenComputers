local util = require("util")
local tasks = require("tasks")
local shell = require("shell")
local wterm = require("wterm")

local args, options = shell.parse(...)
if not options.s and not options.l and not options.c then
    wterm.write("Usage: task [-s|-l|-c]")
    wterm.write(" -s: save tasks to disk.")
    wterm.write(" -l: load tasks from disk.")
    wterm.write(" -c: clear all tasks.")
    return false
end

if util.boolToNumber(options.s) + util.boolToNumber(options.l) + util.boolToNumber(options.c) > 1 then
    wterm.writeError("ERROR", "task", "Passed several mutually exclusive flags")
    return false
end

if options.s then
    tasks.saveTasks()
    wterm.write("Saved tasks to disk")
elseif options.l then
    tasks.loadTasks()
    wterm.write("Loaded tasks from disk")
elseif options.c then
    tasks.clearTasks()
    wterm.write("Cleared active tasks")
end

return true