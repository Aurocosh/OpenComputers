local component = require("component")
local computer = require("computer")
local robot = require("robot")
local shell = require("shell")
local sides = require("sides")
local nav = require("nav")
local tasks = require("tasks")
local miner = require("miner")
local serialization = require("serialization")

if not component.isAvailable("robot") then
    io.stderr:write("can only run on robots")
    return
end

local args, options = shell.parse(...)
if #args < 1 then
  io.write("Usage: tunnel [-s] [-d] <Quarry width> <Quarry depth> [Quarry heigth] \n")
  io.write(" -s: shutdown when done.")
  io.write(" -d: descend.")
  return
end

local quarryWidth = tonumber(args[1])
if not quarryWidth or quarryWidth <= 0 then
  io.stderr:write("invalid quarry width")
  return
end

--local quarryDepth = math.floor(tonumber(args[2])/3)
local quarryDepth = tonumber(args[2])
if not quarryDepth or quarryDepth <= 0 then
  io.stderr:write("invalid quarry depth")
  return
end

local quarryHeigth = tonumber(args[3]) or quarryWidth

local function dropItems()
    for slot = 1, 15 do
        if robot.count(slot) > 0 then
            robot.select(slot)
            local wait = 1
            repeat
                if not robot.drop() and not robot.dropDown() then
                    os.sleep(wait)
                    wait = math.min(10, wait + 1)
                end
            until robot.count(slot) == 0
        end
    end
    robot.select(1)
    return true
end

local function deliverItems()
    local allEmpty = true;
    for i = 14, 15 do
        allEmpty = allEmpty and robot.count(i) == 0
    end
    if allEmpty then
        return false
    end
    
    local x,_,z = nav.getPosition()
    local facing = nav.getFacing()
    
    --tasks.addVoidTask(nav.setSuckItemsWhileMoving,"Stopping item picker",{false})
    tasks.addTask(nav.moveXZ, "Going to deliver items",{0,0})
    tasks.addTask(nav.faceSide, "Turning towards chests",{sides.south})
    tasks.addTask(dropItems, "Dropping items")
    tasks.addTask(nav.moveXZ, "Returning to position",{x,z})
    tasks.addTask(nav.faceSide, "Turning towards old side",{facing})
    --tasks.addVoidTask(nav.setSuckItemsWhileMoving,"Starting item picker",{true})
    return true
end

local function collectTorches()
    robot.select(16)
    robot.suckUp(63)
    local torchCount = robot.count(16)
    if torchCount == 1 then
        tasks.addTask(os.sleep, "No torches, sleeping",{60})
        return false
    end
    robot.select(1)
    return true
end

local function placeTorch()
    robot.select(16)
    local torchCount = robot.count(16)
    if(torchCount > 1) then
        robot.placeDown()
        return true
    elseif torchCount == 1 and options.r then
        local x,_,z = nav.getPosition()
        local facing = nav.getFacing()
               
        --tasks.addVoidTask(nav.setSuckItemsWhileMoving,"Stopping item picker",{false})
        tasks.addTask(nav.moveXZ, "Going to refill torches",{0,0})
        tasks.addTask(nav.faceSide, "Turning towards chests",{sides.south})
        tasks.addTask(collectTorches, "Collecting torches")
        tasks.addTask(nav.moveXZ, "Returning to position",{x,z})
        tasks.addTask(nav.faceSide, "Turning towards old side",{facing})
        --tasks.addVoidTask(nav.setSuckItemsWhileMoving,"Starting item picker",{true})
        return false
    end    
    robot.select(1)
    return true
end


local function digForwardTask(taskData, placeTorches)    
    if taskData.blocksLeft == 0 then
        return true
    end
        
    local blocksLeft = taskData.blocksLeft
    for i = 1, blocksLeft do
        nav.forward()
        miner.clearBlock(sides.down)
        miner.clearBlock(sides.up)
        taskData.blocksLeft = taskData.blocksLeft - 1
                
        if placeTorches and options.t and taskData.blocksLeft%torchSpacing == 0 then
            tasks.addTask(placeTorch, "Placing torch")
            return false;
        end
        if options.d and deliverItems() then    
            return false;
        end
    end
    return true
end

local function digCorridorTask(taskData)    
    nav.turnRight()
    nav.forward() 
    miner.clearBlock(sides.down)
    nav.back()
    nav.turnLeft()
    for i = 1, taskData.blocksLeft do
        nav.forward()
        nav.turnRight()
        nav.forward()
        miner.clearBlock(sides.down)
        nav.back()
        nav.turnLeft()
        miner.clearBlock(sides.down)
    end
    return true
end

local function main()
    nav.setDiggingAllowed(true)
    --nav.setSuckItemsWhileMoving(true)
    
    if(startingTunnel > 1) then
        local x = (startingTunnel - 1) * 4
        
        
        tasks.addTask(nav.moveXZ,"Going to starting tunnel",{x,0})
        tasks.addTask(nav.faceSide, "Turning towards tunnel",{sides.north})
    end
    
    
    for i = startingTunnel, tunnelCount do
        tasks.addTask(placeTorch, "Placing torch ")
        tasks.addTask(digForwardTask, "Digging forward tunnel "..i, {{blocksLeft = tunnelLength}, true})
        tasks.addVoidTask(nav.turnRight,"Turning right")
        tasks.addVoidTask(nav.forward,"Going forward")
        tasks.addVoidTask(miner.clearBlock,"Breaking block above",{sides.up})
        tasks.addVoidTask(miner.clearBlock,"Breaking block below",{sides.down})
        tasks.addVoidTask(nav.turnRight,"Turning right")
        tasks.addTask(digForwardTask,"Digging forward tunnel "..i, {{blocksLeft = tunnelLength}, false})
        tasks.addVoidTask(nav.turnLeft,"Turning left")
        tasks.addTask(digCorridorTask,"Digging corridor", {{blocksLeft = 3}})
        tasks.addVoidTask(nav.turnLeft,"Turning left ")
    end    
    return true
end

tasks.clearAllTasks()
tasks.addDelayedTask(main,"Initializing")
tasks.addDelayedTask(nav.moveXZ,"Going to origin",{0,0})
tasks.addDelayedTask(nav.faceSide,"Turning",{sides.south})
tasks.processAllTasks()

if options.s then
  computer.shutdown()
end