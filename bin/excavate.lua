local component = require("component")
local computer = require("computer")
local robot = require("robot")
local shell = require("shell")
local excavate = require("excavate")
local tasks = require("tasks")
local wterm = require("wterm")
local logf = require("logf")

if not component.isAvailable("robot") then
  wterm.writeError("Can only run on robots")
  return
end

local args, options = shell.parse(...)
if #args < 1 then
    wterm.write("Usage: excavate [-c] [-t] <region>")
    wterm.write(" -c: descend.")
    wterm.write(" -t: add task.")
    return
end

local region = args[1]
if not region then
    io.stderr:write("invalid region")
    return
end

if options.t then
    wterm.write("Excavation task added")
    tasks.addTask("rgisDig", {region})
    return true
end

local excavationCoroutine = coroutine.create(excavate.rgisDig)
local result, reason = coroutine.resume(excavationCoroutine, region)
while coroutine.status(excavationCoroutine) == "suspended" do
    result, reason = coroutine.resume(excavationCoroutine)
end

if not result then
    wterm.writeError("Excavation failed. Reason: "..reason)
    return false
end

--[[
if options.c then
    miner.descend()
    if robot.detectDown() then
        local _,y = nav.getPosition()
        currentLayer = math.floor(math.abs(y)/3)
    end
end
--]]