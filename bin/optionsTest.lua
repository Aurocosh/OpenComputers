package.loaded.wio = nil
package.loaded.wterm = nil
package.loaded.persist = nil

local wio = require("wio")
local wterm = require("wterm")
local shell = require("shell")
local process = require("process")
local persist = require("persist")
local serialization = require("serialization")

local args, options = shell.parse(...)
if false then
  wterm.write("Usage: task [-a] [-r] [-l] [-c]")
  wterm.write(" -a: add task.")
  wterm.write(" -r: remove task.")
  wterm.write(" -l: list all tasks.")
  wterm.write(" -c: clear all tasks.")
  return
end


if options.a then    
    wterm.write(" -a: detected.")
end

if options.b then    
    wterm.write(" -b: detected.")
end

if options.c then    
    wterm.write(" -c: detected.")
end

local arg = args[1] or ""
wterm.write("arg 1 "..arg)
local arg = args[2] or ""
wterm.write("arg 2 "..arg)
local arg = args[3] or ""
wterm.write("arg 3 "..arg)
local arg = args[4] or ""
wterm.write("arg 4 "..arg)
