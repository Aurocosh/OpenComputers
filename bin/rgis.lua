local rgis = require("rgis")
local logf = require("logf")
local util = require("util")
local shell = require("shell")
local wterm = require("wterm")

local args, options = shell.parse(...)
if not options.s and not options.l then
    wterm.write("Usage: rgis [-s|-l]")
    wterm.write(" -s: save regions to disk.")
    wterm.write(" -l: load regions from disk.")
    return false
end

if util.boolToNumber(options.s) + util.boolToNumber(options.l) > 1 then
    wtermError.write("Passed several mutually exclusive flags")
    return false
end

if options.s then
    rgis.saveRegions()
    wterm.write("Saved regions to disk")
else
    rgis.loadRegions()
    wterm.write("Loaded regions from disk")
end

return true