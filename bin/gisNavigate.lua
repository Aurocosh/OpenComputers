local nav = require("nav")
local gis = require("gis")
local logf = require("logf")
local shell = require("shell")
local wterm = require("wterm")
local tasks = require("tasks")
local wsides = require("wsides")
local vector = require("vector")
local component = require("component")
local pathConstruct = require("pathConstruct")

if not component.isAvailable("robot") then
    wterm.writeError("Can only run on robots")
    return false
end

local args, options = shell.parse(...)
if #args < 1 then
    wterm.write("Usage: gisNavigate [-b] <name>")
    wterm.write(" -b: break blocks.")
    wterm.write(" -t: add task.")
    return false
end

local name = args[1]
if not name then
    wterm.writeError("invalid name")
    return false
end

if options.t then
    wterm.write("Navigation task added")
    tasks.addTask("gisNavigate", {name})
    return true
end

nav.setDiggingAllowed(options.b)
local navigationCoroutine = coroutine.create(nav.gisNavigateCoroutine)
local result, reason = coroutine.resume(navigationCoroutine, name)
while coroutine.status(navigationCoroutine) == "suspended" do
    result, reason = coroutine.resume(navigationCoroutine)
end

if not result then
    wterm.writeError("Navigation failed. Reason: "..reason)
    return false
end

return true