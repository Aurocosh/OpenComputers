local box = require("box")
local nav = require("nav")
local rgis = require("rgis")
local shell = require("shell")
local wterm = require("wterm")
local vector = require("vector")

local args, options = shell.parse(...)
if #args == 0 then
    wterm.write("Usage: rgisAddCurrent [-u] [-s] <name> xWidth yWidth zWidth")
    wterm.write(" -u: uniquePosition.")
    wterm.write(" -s: save all coordinates to disk.")
    return false
end

local name = args[1]
if not name then
    wterm.writeError("Invalid name")
    return false
end

local xWidth = tonumber(args[2])
if not xWidth then
    wterm.writeError("Invalid x width")
    return false
end

local yWidth = tonumber(args[3])
if not yWidth then
    wterm.writeError("Invalid y width")
    return false
end

local zWidth = tonumber(args[4])
if not zWidth then
    wterm.writeError("Invalid z width")
    return false
end

local x, y, z = nav.getPosition()
local cornerA = vector.new(x,y,z)
local cornerB = vector.new(x + xWidth, y + yWidth, z + zWidth)
local region = box.new(cornerA, cornerB)
rgis.addRegion(name, region, options.u)
wterm.write("New region added. Name:"..name.." corners: "..tostring(region))

if options.s then
    rgis.saveRegions()
end

return true