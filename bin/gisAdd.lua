local gis = require("gis")
local logf = require("logf")
local shell = require("shell")
local wterm = require("wterm")
local wsides = require("wsides")
local vector = require("vector")
local component = require("component")

local args, options = shell.parse(...)
if #args == 0 then
    wterm.write("Usage: gisAdd [-u] [-s] -- <name> <x> <y> <z>")
    wterm.write(" -u: uniquePosition.")
    wterm.write(" -s: save all coordinates to disk.")
    return false
end

local name = args[1]
if not name then
    wterm.writeError("Invalid name")
    return false
end

local x = tonumber(args[2])
if not x then
    wterm.writeError("Invalid x coordinate")
    return false
end

local y = tonumber(args[3])
if not y then
    wterm.writeError("Invalid y coordinate")
    return false
end

local z = tonumber(args[4])
if not z then
    wterm.writeError("Invalid z coordinate")
    return false
end

local coordinates = vector.new(x,y,z)
gis.addCoordinates(name, coordinates, options.u)
wterm.write("New coordinates added. Name:"..name.." x:"..x.." y:"..y.." z:"..z)

if options.s then
    gis.saveCoordinates()
end

return true