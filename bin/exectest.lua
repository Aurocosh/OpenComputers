
local wterm = require("wterm")
local wshell = require("wshell")
local serialization = require("serialization")
local logf = require("logf")
local thread = require("thread")

local res = {}

wterm.write("Exectest launched")
local main = thread.create(function(name, res)
    wterm.write("Main launched")
    os.sleep(1)
    wterm.write("Main stopped")
    coroutine.yield()
    res[name] = true
    return true
end,"Main",res)

local a = thread.create(function(name, res)
    coroutine.yield()
    wterm.write("A launched")
    os.sleep(3)
    wterm.write("A stopped")
    res[name] = true
    return true
end,"A",res)

local b = thread.create(function(name, res)
    wterm.write("B launched")
    os.sleep(5)
    wterm.write("B stopped")
    res[name] = true
    return true
end,"B",res)



thread.waitForAll({main,a,b})
wterm.writeTable(res)


--[[wterm.write("Exectest launched")
wterm.write("Launching rettest")
    local result,keepAlive,taskData = wshell.execute("rettest",oldData)
--logf.logLine("DEBUG","test", serialization.serialize(_G,100000))
local oldData = {data="Old data"}

wterm.write("Rettest returned "..tostring(result))
wterm.write("Rettest keepAlive "..tostring(keepAlive))
wterm.writeTable(taskData)
--]]