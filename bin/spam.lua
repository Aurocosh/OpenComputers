local nav = require("nav")
local logf = require("logf")
local shell = require("shell")
local wterm = require("wterm")
local wsides = require("wsides")
local vector = require("vector")
local component = require("component")
local pathConstruct = require("pathConstruct")

if not component.isAvailable("modem") then
    wterm.writeError("Only computers with modems can spam")
    return false
end

local args, options = shell.parse(...)
if #args < 4 then
    wterm.write("Usage: -- spam <message> <address> <port> <count>")
    return false
end

local message = args[1]
if not message then
    wterm.writeError("invalid message")
    return false
end

local address = args[2]
if not address then
    wterm.writeError("invalid address")
    return false
end

local port = tonumber(args[3])
if not port then
    wterm.writeError("invalid port")
    return false
end

local count = tonumber(args[4])
if not count then
    wterm.writeError("invalid count")
    return false
end

local modem = component.modem
local strength = modem.getStrength()
modem.setStrength(400)

wterm.write("Spamming "..message)
for i=1,count do 
    modem.send(address,port,message)
end
modem.setStrength(strength)

return true