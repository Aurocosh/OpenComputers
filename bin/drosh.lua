local event = require("event")
local shell = require("shell")
local tty = require("tty")
local text = require("text")
local thread = require("thread")
local sh = require("sh")

local args, options = shell.parse(...)

shell.prime()
local needs_profile = io.input().tty
local has_prompt = needs_profile and io.output().tty and not options.c
local input_handler = {hint = sh.hintHandler}

local mainThread
local commands = {}
local threadResult = {}

local function main(threadName)
    io.write("Main thread started\n")
    print("Main thread started\n")
    while true do
        print("Main thread\n")
        if #commands > 0 then
            print("Command added\n")
            command = table.remove(commands)
            local result, reason = sh.execute(_ENV, command)
            --threadResult[threadName] = {result = result, reason = reason}
        else
            os.sleep(5)
        end
    end
end

local function interface(threadName)
    while true do
        if has_prompt then
            while not tty.isAvailable() do
                event.pull("term_available")
            end
            if needs_profile then -- first time run AND interactive
                needs_profile = nil
                dofile("/etc/profile.lua")
            end
            io.write(sh.expand(os.getenv("PS1") or "$ "))
        end
        local command = tty.read(input_handler)
        if command then
            command = text.trim(command)
            if command == "exit" then
                threadResult[threadName] = {action = "exit"}
                return
            elseif command ~= "" then
                threadResult[threadName] = {action = "command", command = command}
                return
            end
        elseif command == nil then -- false only means the input was interrupted
            threadResult[threadName] = {action = "exit"}
            return -- eof
        end
        if has_prompt and tty.getCursor() > 1 then
            io.write("\n")
        end
    end
end

if #args == 0 then
    mainThread = thread.create(main,"main")
    while true do
        local interfaceThread = thread.create(interface,"interface")
        thread.waitForAll({interfaceThread})
        local result = threadResult["interface"]
        threadResult["interface"] = nil
        if result.action == "exit" then
            print("Exit\n")
            mainThread:kill()
            return
        elseif result.action == "command" then
            table.insert(commands, result.command)
        end
    end
else
    -- execute command.
    local result = table.pack(sh.execute(...))
    if not result[1] then
        error(result[2], 0)
    end
    return table.unpack(result, 2)
end
