local gis = require("gis")
local box = require("box")
local logf = require("logf")
local shell = require("shell")
local wterm = require("wterm")
local wsides = require("wsides")
local vector = require("vector")
local component = require("component")

local args, options = shell.parse(...)
if #args == 0 then
    wterm.write("Usage: rgisAdd [-u] [-s] -- <name> <ax> <ay> <az> <bx> <by> <bz>")
    wterm.write(" -u: not unique region.")
    wterm.write(" -s: save all regions to disk.")
    return false
end

local name = args[1]
if not name then
    wterm.writeError("Invalid name")
    return false
end

local x = tonumber(args[2])
if not x then
    wterm.writeError("Invalid a.x coordinate")
    return false
end

local ay = tonumber(args[3])
if not ay then
    wterm.writeError("Invalid a.y coordinate")
    return false
end

local az = tonumber(args[4])
if not az then
    wterm.writeError("Invalid a.z coordinate")
    return false
end

local bx = tonumber(args[5])
if not bx then
    wterm.writeError("Invalid b.x coordinate")
    return false
end

local by = tonumber(args[6])
if not by then
    wterm.writeError("Invalid b.y coordinate")
    return false
end

local bz = tonumber(args[7])
if not bz then
    wterm.writeError("Invalid b.z coordinate")
    return false
end

local cornerA = vector.new(ax, ay, az)
local cornerB = vector.new(bx, by, bz)
local region = box.new(cornerA, cornerB)
rgis.addRegion(name, region, options.u)
wterm.write("New region added. Name:"..name)

if options.s then
    rgis.saveRegions()
end

return true