package.loaded.hio = nil
package.loaded.persist = nil

local hio = require("hio")
local term = require("term")
local process = require("process")
local persist = require("persist")
local serialization = require("serialization")

local testTable = {}
testTable.name = "Tom"
testTable.numbers = {1,2,34,4,5}
testTable.int = 6
testTable.bool = true

--[[
hio.saveTableToFile("/data/test/data/file",testTable,true)

local data = hio.readFile("/data/test/data/file")
term.write(data)
term.write(serialization.serialize(data,true))
--]]


--term.write("ret info "..serialization.serialize(process.info().command,true).."\n")