local wterm = require("wterm")
local process = require("process")
local serialization = require("serialization")

wterm.write("Rettest launched")
wterm.write("Current task data")
wterm.writeTable(taskData)
wterm.write("Returning")

taskData = {}
taskData.state = "ok"
taskData.num = 123


keepAlive = true
--term.write("ret info "..serialization.serialize(process.info(),true).."\n")
--term.write("parent info "..serialization.serialize(process.info(2),true).."\n")



return true