local rgis = require("rgis")
local wterm = require("wterm")

wterm.write("Region list")
local regionList = rgis.getAllRegions()
for k,v in pairs(regionList) do    
    wterm.write("Name:"..k)
    for i,c in pairs(v) do
        wterm.write(i..") "..tostring(c))
    end
    wterm.newLine()
end

return true