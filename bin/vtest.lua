package.loaded.vector = nil

local logf = require("logf")
local vector = require("vector")
local serialization = require("serialization")

logf.line("INFO","vtest","Testing vector lib")

local vectorA = vector.new(1,3,4)
logf.line("INFO","vtest","Vector A construction (1,3,4)")
logf.table("INFO","vtest"," %s",vectorA)

local vectorB = vector.new(5,4,3)
logf.line("INFO","vtest","Vector B construction (5,4,3)")
logf.table("INFO","vtest"," %s",vectorB)

local vector = vectorA + vectorB
logf.line("INFO","vtest","A + B")
logf.table("INFO","vtest"," %s",vector)

local vector = vectorA - vectorB
logf.line("INFO","vtest","A - B")
logf.table("INFO","vtest"," %s",vector)

local vector = vectorA * 2
logf.line("INFO","vtest","A * 2")
logf.table("INFO","vtest"," %s",vector)

local vector = vectorA / 2
logf.line("INFO","vtest","A / 2")
logf.table("INFO","vtest"," %s",vector)

local vector = vectorB * 2
logf.line("INFO","vtest","B * 2")
logf.table("INFO","vtest"," %s",vector)

local vector = vectorB / 2
logf.line("INFO","vtest","B / 2")
logf.table("INFO","vtest"," %s",vector)

logf.line("INFO","vtest","#A")
logf.table("INFO","vtest"," %s",#vectorA)

logf.line("INFO","vtest","#B")
logf.table("INFO","vtest"," %s",#vectorB)

logf.line("INFO","vtest","A == B")
logf.table("INFO","vtest"," %s",vectorA == vectorB)

logf.line("INFO","vtest","A == A")
logf.table("INFO","vtest"," %s",vectorA == vectorA)

logf.line("INFO","vtest","-A")
logf.table("INFO","vtest"," %s",-vectorA)



local vector = vectorA:add(vectorB)
logf.line("INFO","vtest","A + B")
logf.table("INFO","vtest"," %s",vector)

local vector = vectorA:sub(vectorB)
logf.line("INFO","vtest","A - B")
logf.table("INFO","vtest"," %s",vector)

local vector = vectorA:mul(2)
logf.line("INFO","vtest","A * 2")
logf.table("INFO","vtest"," %s",vector)

local vector = vectorA:div(2)
logf.line("INFO","vtest","A / 2")
logf.table("INFO","vtest"," %s",vector)

local vector = vectorB:mul(2)
logf.line("INFO","vtest","B * 2")
logf.table("INFO","vtest"," %s",vector)

local vector = vectorB:div(2)
logf.line("INFO","vtest","B / 2")
logf.table("INFO","vtest"," %s",vector)

logf.line("INFO","vtest","#A")
logf.table("INFO","vtest"," %s",vectorA:length())

logf.line("INFO","vtest","#B")
logf.table("INFO","vtest"," %s",vectorB:length())

logf.line("INFO","vtest","A == B")
logf.table("INFO","vtest"," %s",vectorA:equals(vectorB))

logf.line("INFO","vtest","A == A")
logf.table("INFO","vtest"," %s",vectorA:equals(vectorA))

logf.line("INFO","vtest","-A")
logf.table("INFO","vtest"," %s",vectorA:neg())

local vector = vectorA:dot(vectorB)
logf.line("INFO","vtest","A dot B")
logf.table("INFO","vtest"," %s",vector)

local vector = vectorA:cross(vectorB)
logf.line("INFO","vtest","A cross B")
logf.table("INFO","vtest"," %s",vector)


local vector = vectorA:normalize()
logf.line("INFO","vtest","A norm")
logf.table("INFO","vtest"," %s",vector)

io.write("\n")