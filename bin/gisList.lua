local gis = require("gis")
local wterm = require("wterm")

wterm.write("Coordinates list")
local coordinateList = gis.getAllCoordinates()
for k,v in pairs(coordinateList) do    
    wterm.write("Name:"..k)
    for i,c in pairs(v) do
        wterm.write(i..") "..tostring(c))
    end
    wterm.newLine()
end

return true