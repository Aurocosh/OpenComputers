local gis = require("gis")
local nav = require("nav")
local shell = require("shell")
local wterm = require("wterm")
local vector = require("vector")

local args, options = shell.parse(...)
if #args == 0 then
    wterm.write("Usage: gisAddCurrent [-u] [-s] <name>")
    wterm.write(" -u: uniquePosition.")
    wterm.write(" -s: save all coordinates to disk.")
    return false
end

local name = args[1]
if not name then
    wterm.writeError("Invalid name")
    return false
end

local x, y, z = nav.getPosition()
local coordinates = vector.new(x,y,z)
gis.addCoordinates(name, coordinates, options.u)
wterm.write("New coordinates added. Name:"..name.." x:"..x.." y:"..y.." z:"..z)

if options.s then
    gis.saveCoordinates()
end

return true