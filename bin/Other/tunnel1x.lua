local component = require("component")
local computer = require("computer")
local robot = require("robot")
local shell = require("shell")
local sides = require("sides")
local serialization = require("serialization")
local tasks = {}

if not component.isAvailable("robot") then
  io.stderr:write("can only run on robots")
  return
end

local args, options = shell.parse(...)
if #args < 1 then
  io.write("Usage: tunnel [-s] [-t] [-r] [-d] <tunnel count> <tunnel length> [torch spacing] \n")
  io.write(" -s: shutdown when done.")
  io.write(" -t: dont place torches.")
  io.write(" -r: dont refill torches.")
  io.write(" -d: dont dropoff items.")
  return
end

local tunnelCount = tonumber(args[1])
if not tunnelCount then
  io.stderr:write("invalid tunnel count")
  return
end

local tunnelLength = tonumber(args[2])
if not tunnelLength then
  io.stderr:write("invalid tunnel length")
  return
end

local torchSpacing = tonumber(args[3]) or 10

local r = component.robot
local x, y, z, f = 0, 0, 0, 0
local dropping = false -- avoid recursing into drop()
local delta = {[0] = function() x = x + 1 end, [1] = function() y = y + 1 end,
               [2] = function() x = x - 1 end, [3] = function() y = y - 1 end}
               
local immediateTasks = {};
local queuedTasks = {};

local function turnRight()
  robot.turnRight()
  f = (f + 1) % 4
end

local function turnLeft()
  robot.turnLeft()
  f = (f - 1) % 4
end

local function turnTowards(side)
  if f == side - 1 then
    turnRight()
  else
    while f ~= side do
      turnLeft()
    end
  end
end

local function clearBlock(side, cannotRetry)
  while r.suck(side) do
  end
  local result, reason = r.swing(side)
  if not result then
  else
    local _, what = r.detect(side)
    if cannotRetry and what ~= "air" and what ~= "entity" then
      return false
    end
  end
  return true
end

local function tryMove(side)
  side = side or sides.forward
  local tries = 10
  while not r.move(side) do
    tries = tries - 1
    if not clearBlock(side, tries < 1) then
      return false
    end
  end
  if side == sides.down then
    z = z + 1
  elseif side == sides.up then
    z = z - 1
  else
    delta[f]()
  end
  return true
end

local function moveTo(tx, ty, tz, backwards)
  local axes = {
    function()
      while z > tz do
        tryMove(sides.up)
      end
      while z < tz do
        tryMove(sides.down)
      end
    end,
    function()
      if y > ty then
        turnTowards(3)
        repeat tryMove() until y == ty
      elseif y < ty then
        turnTowards(1)
        repeat tryMove() until y == ty
      end
    end,
    function()
      if x > tx then
        turnTowards(2)
        repeat tryMove() until x == tx
      elseif x < tx then
        turnTowards(0)
        repeat tryMove() until x == tx
      end
    end
  }
  if backwards then
    for axis = 3, 1, -1 do
      axes[axis]()
    end
  else
    for axis = 1, 3 do
      axes[axis]()
    end
  end
end

local function step()
    if not tryMove() then
        return false
    end
    clearBlock(sides.down)
    clearBlock(sides.up)
    return true
end

local function turn(i)
  if i % 2 == 1 then
    turnRight()
  else
    turnLeft()
  end
end

local function isDropoffNeeded()
    local allEmpty = true;
    for i = 14, 15 do
        allEmpty = allEmpty and robot.count(i) == 0
    end
    if not allEmpty then
        return true
    end
    return false
end

local function moveTask(taskData)
    local length = taskData ~= nil and taskData.length or 1
    for i = 1, length do
        if not tryMove() then
            return true,"Unable to move"
        end
    end
    return true
end

local function stepTask(taskData)
    local length = taskData ~= nil and taskData.length or 1
    for i = 1, length do
        if not step() then
            return true,"Unable to step"
        end
    end
    return true
end

local function movementTask(taskData)
    moveTo(taskData.x, taskData.y, taskData.z)
    return true
end

local function turnTowardsTask(taskData)
    turnTowards(taskData.side)
    return true
end

local function sleepTask(taskData) 
    os.sleep(taskData.seconds)
    return true
end

local function dropItemsTask(taskData)
    for slot = 1, 15 do
        if robot.count(slot) > 0 then
            robot.select(slot)
            local wait = 1
            repeat
                if not robot.drop() and not robot.dropDown() then
                    os.sleep(wait)
                    wait = math.min(10, wait + 1)
                end
            until robot.count(slot) == 0
        end
    end
    robot.select(1)
    return true
end

local function itemDropOffTask(taskData)
    table.insert(immediateTasks, {func = movementTask, message = "Moving to chests", data = {x=-1,y=0,z=0}});  
    table.insert(immediateTasks, {func = turnTowardsTask, message = "Turning towards chests", data = {side = 2}});
    table.insert(immediateTasks, {func = dropItemsTask, message = "Dropping items"});
    table.insert(immediateTasks, {func = movementTask, message = "Returning to position ", data = {x=x,y=y,z=z}});
    table.insert(immediateTasks, {func = turnTowardsTask, message = "Turning towards old side", data = {side = f}});
    return true
end




local function collectTorchesTask(taskData)
    robot.select(16)
    robot.suckUp(63)
    local torchCount = robot.count(16)
    if torchCount == 1 then
        table.insert(immediateTasks, {func = sleepTask, message = "No torches, sleeping", data = {seconds = 10}});
        return false
    end
    robot.select(1)
    return true
end

local function refillTorchesTask(taskData) 
    table.insert(immediateTasks, {func = movementTask, message = "Moving to chests", data = {x=-1,y=0,z=0}});  
    table.insert(immediateTasks, {func = turnTowardsTask, message = "Turning towards chests", data = {side = 2}});
    table.insert(immediateTasks, {func = collectTorchesTask, message = "Collecting torches "});
    table.insert(immediateTasks, {func = movementTask, message = "Returning to position ", data = {x=x,y=y,z=z}});
    table.insert(immediateTasks, {func = turnTowardsTask, message = "Turning towards old side", data = {side = f}});
    return true
end

local function placeTorchTask(taskData)
    robot.select(16)
    local torchCount = robot.count(16)
    if(torchCount > 1) then
        robot.placeDown()
        return true
    end
    if torchCount == 1 and not options.r then
        table.insert(immediateTasks, {func = refillTorchesTask, message = "Refilling torches "});  
        return false
    end    
    robot.select(1)
    return true
end

local function turnRightTask(taskData)
    turnRight()
    return true
end

local function turnLeftTask(taskData)
    turnLeft()
    return true
end

local function digForwardTask(taskData)    
    if taskData.blocksLeft == 0 then
        return true
    end
        
    local blocksLeft = taskData.blocksLeft
    for i = 1, blocksLeft do
        if not tryMove() then
            return true,"Unable to move"
        end
        clearBlock(sides.down)
        clearBlock(sides.up)
        taskData.blocksLeft = taskData.blocksLeft - 1
                
        if taskData.placeTorches and not options.t and taskData.blocksLeft%torchSpacing == 0 then
            table.insert(immediateTasks, {func = placeTorchTask, message = "Placing torch "});
            return false;
        end
        if not options.d and isDropoffNeeded() then
            table.insert(immediateTasks, {func = itemDropOffTask, message = "Running out of space. Dropping off items"});            
            return false;
        end
    end
    return true
end

local function initializeTask(taskData)
    for i = 1, tunnelCount do
        table.insert(queuedTasks, {func = digForwardTask, message = "Digging forward tunnel "..i, data = {blocksLeft = tunnelLength, placeTorches = true}});
        table.insert(queuedTasks, {func = turnRightTask, message = "Turning right"});
        table.insert(queuedTasks, {func = stepTask, message = "Stepping"});
        table.insert(queuedTasks, {func = turnRightTask, message = "Turning right"});
        table.insert(queuedTasks, {func = digForwardTask, message = "Digging revers tunnel "..i, data = {blocksLeft = tunnelLength, placeTorches = false}});
        table.insert(queuedTasks, {func = turnLeftTask, message = "Turning left"});
        table.insert(queuedTasks, {func = stepTask, message = "Digging corridor ", data = {length = 3}});
        table.insert(queuedTasks, {func = turnLeftTask, message = "Turning left"});
    end    
    return true
end

table.insert(tasks, {func = initializeTask, message = "Initializing"});  

while(#tasks > 0) do 
    local task = tasks[1]
    
    --io.write(#task.."\n")
    --io.write("tasks"..serialization.serialize(tasks,true).."\n")
    if task.message ~= nil then
        io.write(task.message.."\n")
    end
    local completed,errorMessage = task.func(task.data)
    if errorMessage ~= nil then
    
    end
    if completed then
        table.remove(tasks, 1);
    end
    
    for i = #immediateTasks, 1, -1 do
        table.insert(tasks,1,immediateTasks[i])        
    end
    immediateTasks = {}
    for _,v in pairs(queuedTasks) do
        table.insert(tasks,v)
        --io.write("table"..serialization.serialize(queued,true).."\n")
    end
    queuedTasks = {}
end

if options.s then
  computer.shutdown()
end