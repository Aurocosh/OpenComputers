local nav = require("nav")
local gps = require("gps")
local logf = require("logf")
local util = require("util")
local shell = require("shell")
local wterm = require("wterm")
local wsides = require("wsides")
local pflocate = require("pflocate")
local component = require("component")

local args, options = shell.parse(...)
if not options.i and not options.u and not options.s and not options.p then
    wterm.write("Usage: position [-i|-u|-s|-p] [x] [y] [z] [facing]")
    wterm.write(" -i: print current position and facing.")
    wterm.write(" -u: update current position and facing.")
    wterm.write(" -p: determine position via gps.")
    return false
end

if util.boolToNumber(options.u) + util.boolToNumber(options.i) + util.boolToNumber(options.s) + util.boolToNumber(options.p) > 1 then
    logf.line("ERROR", "position", "Passed several mutually exclusive flags")
    return false
end

if options.u and not component.isAvailable("robot") then
    logf.line("ERROR", "position", "Position can only autoupdate on robots")
    return false
end

if options.p then
    local x, y, z = gps.locate()
    if x == nil then
        logf.line("WARNING", "position", "Failed to determine position")
        return true    
    end
    wterm.write("Position: x:"..x.." y:"..y.." z:"..z)
    return true
end

if options.i then
    local x, y, z = nav.getPosition()
    local f = nav.getFacing()
    local facing = wsides[f]
    wterm.write("Position: x:"..x.." y:"..y.." z:"..z.." facing:"..facing)
    return true
end

if options.u then
    local x, y, z = nav.getPosition()
    local f = nav.getFacing()
    local facing = wsides[f]

    local result, err = pflocate.update()
    if not result then
        logf.line("ERROR", "position", "Unable to update position info: "..err)
        return false
    end
    
    local nx, ny, nz = nav.getPosition()
    local nf = nav.getFacing()
    local newFacing = wsides[nf]
    
    wterm.write("Position updated")
    wterm.write("Position old: x:"..x.." y:"..y.." z:"..z.." facing:"..facing)
    wterm.write("Position new: x:"..nx.." y:"..ny.." z:"..nz.." facing:"..newFacing)
    return true
end

return true